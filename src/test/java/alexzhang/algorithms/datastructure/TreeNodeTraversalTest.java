package alexzhang.algorithms.datastructure;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TreeNodeTraversalTest {
    TreeNode root;
    List<Integer> expected;

    @BeforeEach
    public void setup() {
        root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right = new TreeNode(3);
        root.right.right = new TreeNode(6);
    }

    @Test
    public void preorderTraversalRecursiveTest() {
        expected = Arrays.asList(1, 2, 4, 5, 3, 6);
        List<Integer> ans = TreeNodeTraversal.preorderTraversalRecursive(root);
        assertEquals(expected, ans);
    }

    @Test
    public void preorderTraversalNonRecursiveTest() {
        expected = Arrays.asList(1, 2, 4, 5, 3, 6);
        List<Integer> ans = TreeNodeTraversal.preorderTraversalNonRecursive(root);
        assertEquals(expected, ans);
    }

    @Test
    public void inorderTraversalNonRecursiveTest() {
        expected = Arrays.asList(4, 2, 5, 1, 3, 6);
        List<Integer> ans = TreeNodeTraversal.inorderTraversalNonRecursive(root);
        assertEquals(expected, ans);
    }

    @Test
    public void inorderTraversalRecursiveTest() {
        expected = Arrays.asList(4, 2, 5, 1, 3, 6);
        List<Integer> ans = TreeNodeTraversal.inorderTraversalRecursive(root);
        assertEquals(expected, ans);
    }

    @Test
    public void postorderTraversalRecursiveTest() {
        expected = Arrays.asList(4, 5, 2, 6, 3, 1);
        List<Integer> ans = TreeNodeTraversal.postorderTraversalRecursive(root);
        assertEquals(expected, ans);
    }
    @Test
    public void postorderTraversalNonRecursiveTest() {
        expected = Arrays.asList(4, 5, 2, 6, 3, 1);
        List<Integer> ans = TreeNodeTraversal.postorderTraversalNonRecursive(root);
        assertEquals(expected, ans);
    }
    @Test
    public void postorderTraversalNonRecursive2Test() {
        expected = Arrays.asList(4, 5, 2, 6, 3, 1);
        List<Integer> ans = TreeNodeTraversal.postorderTraversalNonRecursive2(root);
        assertEquals(expected, ans);
    }

    @Test
    public void t() {
        root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.right = new TreeNode(3);
        List<Integer> integers = TreeNodeTraversal.preorderTraversalRecursive(root);
        System.out.println(integers);
        root = new TreeNode(1);
        root.right = new TreeNode(3);
        root.right.left = new TreeNode(2);
        integers = TreeNodeTraversal.preorderTraversalRecursive(root);
        System.out.println(integers);
    }
}