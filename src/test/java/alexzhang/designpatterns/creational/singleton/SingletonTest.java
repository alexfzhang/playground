package alexzhang.designpatterns.creational.singleton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Alex Zhang
 */

class SingletonTest {

    @Test
    public void testSingletonEarly() {
        Early early = Early.getInstance();
        early.doSomething();
        Early early2 = Early.getInstance();
        early2.doSomething();
        Assertions.assertSame(early, early2);
    }
    @Test
    public void testSingletonLazy() {
        Lazy lazy = Lazy.getInstance();
        lazy.doSomething();
        Lazy lazy2 = Lazy.getInstance();
        lazy2.doSomething();
        Assertions.assertSame(lazy, lazy2);
    }

    @Test
    public void testSingletonLazyThreadSafe() {
        SingletonThreadSafe ts = SingletonThreadSafe.getInstance();
        ts.doSomething();
        SingletonThreadSafe ts2 = SingletonThreadSafe.getInstance();
        ts2.doSomething();
        Assertions.assertSame(ts, ts2);
    }
}