package alexzhang.online.sort;

import alexzhang.algorithms.sort.Sort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;
import org.json.XML;

class SortTest {

    private static class Input<T extends Comparable<T>> {
        T[] input;
        T[] expected;

        public Input(T[] input, T[] expected) {
            this.input = input;
            this.expected = expected;
        }

        @Override
        public String toString() {
            return "Input{" +
                    "input=" + Arrays.toString(input) +
                    ", expected=" + Arrays.toString(expected) +
                    '}';
        }
    }

    private static List<Input<Integer>> createInput() {
        Integer[] r = new Integer[]{1, 2, 3, 4, 5};
        Integer[] a1 = new Integer[]{2, 4, 3, 1, 5};
        Integer[] a2 = new Integer[]{1, 2, 3, 4, 5};
        Integer[] a3 = new Integer[]{3, 5, 4, 2, 1};
        Integer[] a4 = new Integer[]{2, 1, 3, 5, 4};
        Integer[] a5 = new Integer[]{5, 2};
        Integer[] a6 = new Integer[]{2};
        List<Input<Integer>> rs = new ArrayList<>();
        rs.add(new Input<>(a1, r));
        rs.add(new Input<>(a2, r));
        rs.add(new Input<>(a3, r));
        rs.add(new Input<>(a4, r));
        rs.add(new Input<>(a5, new Integer[]{2, 5}));
        rs.add(new Input<>(a6, new Integer[]{2}));
        rs.add(new Input<>(new Integer[1], new Integer[1]));
        return rs;
    }

    private static List<Input<Integer>> createInputDesc() {
        Integer[] r = new Integer[]{1, 2, 3, 4, 5};
        Integer[] a1 = new Integer[]{5, 4, 3, 2, 1};
        Integer[] a2 = new Integer[]{1, 2, 3, 4, 5};
        Integer[] a3 = new Integer[]{3, 5, 4, 2, 1};
        Integer[] a4 = new Integer[]{2, 1, 3, 5, 4};
        Integer[] a5 = new Integer[]{2, 5};
        Integer[] a6 = new Integer[]{2};
        List<Input<Integer>> rs = new ArrayList<>();
        rs.add(new Input<>(a1, r));
        rs.add(new Input<>(a2, r));
        rs.add(new Input<>(a3, r));
        rs.add(new Input<>(a4, r));
        rs.add(new Input<>(a5, new Integer[]{5, 2}));
        rs.add(new Input<>(a6, new Integer[]{2}));
        rs.add(new Input<>(new Integer[1], new Integer[1]));
        return rs;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    void quickSortAsc(Input<Integer> input) {
        Sort.quickSort(input.input, 0, input.input.length - 1, 0);
        Assertions.assertArrayEquals(input.expected, input.input);
    }

    @ParameterizedTest
    @MethodSource("createInputDesc")
    void quickSortDesc(Input<Integer> input) {
        Sort.quickSort(input.input, 0, input.input.length - 1, 1);
        Assertions.assertArrayEquals(input.expected, input.input);
    }

    @Test
    void jsonTest() {
        String xml = "<?xml version=\"1.0\" ?><test attra=\"A\" attrib=\"moretest\"><h>H</h><array>1</array><array>2</array><array>3</array><p><h1>H1</h1><h2>H2</h2></p></test>";
        JSONObject j = XML.toJSONObject(xml);
        String jsonPrettyPrintString = j.toString(2);
        System.out.println(jsonPrettyPrintString);
    }
}