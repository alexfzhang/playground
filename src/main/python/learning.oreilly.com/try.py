import urllib3

http = urllib3.PoolManager()

def tryout():
  url = r'https://cfvod.kaltura.com/scf/hls/p/1926081/sp/192608100/serveFlavor/entryId/1_ccuxw89p/v/1/ev/8/flavorId/1_3jhy17nc/name/a.mp4/index.m3u8?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZnZvZC5rYWx0dXJhLmNvbS9zY2YvaGxzL3AvMTkyNjA4MS9zcC8xOTI2MDgxMDAvc2VydmVGbGF2b3IvZW50cnlJZC8xX2NjdXh3ODlwL3YvMS9ldi84KiIsIkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTYzMTY5NTU4OH19fV19&Signature=ZGQC0rHphi2HyOp0K-oC1dGsLEVFDyUFgX6XAqOzCiz2jD2zDInFhZETkNFVSh-GFYrV-NeeiGuqboQT8oRcCZTKBHDDEY-cYLoIh3qwKGOpHTI-VLLU7U5jutbZHeacktRT6-8YYPPhnxMWHrpYip8kVzxCLWWzReYiTbLasU~~G~qfOc9Rm6DKo6hAubtF30MgVCK1dnBoqPYyYrgimSvSljOzQHPi--DxLzSqrVQiGI9oWGpaF76S6PRjxYovtJk5jYtMnhUF4vF4KhHVu-G8kUvc-c6Dw72Vv-tSMIfOAcMiPDUVdbmO8--L6AAcuhHWUcog0SQk-C1EPfsVhA__&Key-Pair-Id=APKAJT6QIWSKVYK3V34A'
  res = http.request('GET', url)
  if res.status == 200:
    ts = filter(lambda item: item.startswith('https'), res.data.decode('utf-8').split('\n'))
    for turl in ts:
      res = http.request('GET', turl)
      if res.status == 200:
        print(turl.split('?')[0])
        with open('c.ts', 'ba') as f:
          f.write(res.data)

if __name__ == '__main__':
  tryout()
