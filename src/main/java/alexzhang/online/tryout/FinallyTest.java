package alexzhang.online.tryout;

/**
 * @author Alex F Zhang
 */
public class FinallyTest {
    public static void main(String[] args) {
        try {
            System.out.println("xxx");
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("catch");
            // if use System.exit(0); then finanlly block won't be executed.
            return;
        } finally {
            System.out.println("finally");
        }
    }
}
