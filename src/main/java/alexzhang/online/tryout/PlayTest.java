package alexzhang.online.tryout;


import org.junit.jupiter.api.Test;
import alexzhang.algorithms.datastructure.TreeNode;

/**
 * @author alexzhang
 */
public class PlayTest {

    @Test
    public void serialSum() {
        final int base = 100;
        for (int n = 1; n <= base; n++) {
            int m = (int) Math.floor((Math.sqrt(1 + 8 * n) - 1) / 2);
            int sum, c;
            StringBuilder f = new StringBuilder();
            System.out.println("=============================================================");
            for (int i = 1; i <= m; i++) {
                for (int j = 1; j <= n; j++) {
                    sum = j;
                    c = j;
                    f.setLength(0);
                    f.append(n).append("=");
                    f.append(c);
                    for (int k = 1; k < i; k++) {
                        c += 1;
                        sum += c;
                        f.append("+").append(c);
                    }
                    if (sum == n) {
                        System.out.println(f);
                        break;
                    }
                }
            }
        }
    }

    @Test
    public void stringBuilderTest() {
        StringBuilder stringBuilder = new StringBuilder("hello");
        stringBuilder.setLength(0);
        System.out.println(stringBuilder);
    }

    private void f(int... values) {
        for (int val : values) {
            System.out.println(val);
        }
    }

    private void create(int flag, int... values) {
        f(values);
    }

    @Test
    public void p() {
        TreeNode n = new TreeNode();
        create(1, 1, 2, 3, 4);
        double a = 1;
        double b = 2;
        System.out.println(a / b);
    }

    @Test
    public void t1() {
        for(char c = '1'; c <= '9';c++) {
            System.out.println(c);
        }
    }
}

