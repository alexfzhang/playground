package alexzhang.online.math;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author alexzhang
 */
public class CalTest {
    public static int add(int a, int b) {
        if(b == 0) {
            return a;
        }
        int sum = a ^ b;
        int carry = (a&b) << 1;
        return add(sum, carry);
    }

    @Test
    public void test() {
        int add = add(5, 8);
        Assertions.assertEquals(13, add);
    }
}
