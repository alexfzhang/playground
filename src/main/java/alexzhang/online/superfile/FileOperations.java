package alexzhang.online.superfile;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @author alexzhang
 */
public class FileOperations {

    public static boolean deleteDirectory(Path p) {
        try {
            Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if (!Files.isWritable(file)) {
                        if (!file.toFile().setWritable(true)) {
                            throw new IOException("Permission Denied");
                        }
                    }
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    if (null == exc) {
                        if (!Files.isWritable(dir)) {
                            throw new IOException("Permission Denied");
                        }
                    } else {
                        throw exc;
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static boolean copyFileTree(Path src, Path dest) {
        try {
            Files.walkFileTree(src, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    try {
                        Files.copy(src, dest.resolve(src.relativize(dir)));
                    } catch (FileAlreadyExistsException e) {
                        if (!Files.isDirectory(dest)) {
                            throw e;
                        }
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Path destFile = dest.resolve(src.relativize(file));
                    if (Files.notExists(destFile)) {
                        Files.copy(file, destFile);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
