package alexzhang.designpatterns.creational.singleton;

/**
 * 双检锁/双重校验锁（DCL，即 double-checked locking）
 * 是否 Lazy 初始化：是
 * 是否多线程安全：是
 * 描述：这种方式采用双锁机制，安全且在多线程情况下能保持高性能。
 * getInstance() 的性能对应用程序很关键。
 *
 * @author Alex F Zhang
 */
public class SingletonThreadSafe {
    private static volatile SingletonThreadSafe instance;

    private SingletonThreadSafe() {
        // prevent reflection creating new instance
        if (null != instance) {
            throw new RuntimeException("Use getInstance() method to get instance");
        }
    }

    public static SingletonThreadSafe getInstance() {
        if (null == instance) {
            synchronized (SingletonThreadSafe.class) {
                if (null == instance) {
                    instance = new SingletonThreadSafe();
                }
            }
        }
        return instance;
    }

    public void doSomething() {
        System.out.println("Do Something");
    }
}
