package alexzhang.designpatterns.creational.singleton;

/**
 * Lazy instantiation mode/懒汉模式
 * 是否 Lazy 初始化：是
 * 是否多线程安全：否
 *
 * @author Alex F Zhang
 */
public class Lazy {
    private static Lazy instance;

    private Lazy() {
        // prevent reflection creating new instance
        if (null != instance) {
            throw new RuntimeException("Use getInstance() method to get instance");
        }
    }

    public static Lazy getInstance() {
        if (null == instance) {
            instance = new Lazy();
        }
        return instance;
    }

    public void doSomething() {
        System.out.println("Do Something");
    }
}
