package alexzhang.tableclone;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author Alex F Zhang
 */
public class ConfUtil {
    public static Map<String, Object> getConf(String confFile) {
        try {
            InputStream resourceAsStream = Clone.class.getClassLoader().getResourceAsStream(confFile);
            assert resourceAsStream != null;
            String confStr = IOUtils.toString(resourceAsStream);
            return new JSONObject(confStr).toMap();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
