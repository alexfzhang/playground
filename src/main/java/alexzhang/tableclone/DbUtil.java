package alexzhang.tableclone;


import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

/**
 * jdbc:oracle:thin:@localhost:1521:xe
 *
 * @author Alex F Zhang
 */
public class DbUtil {
    private static DataSource ds;

    private DbUtil(String propertyFile) throws Exception {
        ds = OraclePool.getPool(propertyFile);
    }

    private DbUtil(Map<String, String> properties) {
        ds = OraclePool.getPool(properties);
    }

    public static DbUtil getDbUtil(String dbPropertiesFile) {
        try {
            return new DbUtil(dbPropertiesFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static DbUtil getDbUtil(Map<String, String> properties) {
        try {
            return new DbUtil(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Object> getTableMeta(String owner, String tname) {
        final String selectColumns = "SELECT COLUMN_ID,COLUMN_NAME,DATA_TYPE FROM SYS.ALL_TAB_COLS WHERE OWNER=? AND TABLE_NAME=?";
        final String selectConstraintNames = "SELECT CONSTRAINT_NAME, CONSTRAINT_TYPE FROM USER_CONSTRAINTS WHERE OWNER=? AND TABLE_NAME=? " +
                "AND CONSTRAINT_TYPE IN ('P', 'U')";
        final String selectConstraintColumns = "SELECT COLUMN_NAME FROM SYS.USER_CONS_COLUMNS WHERE OWNER=? AND TABLE_NAME=? " +
                "AND CONSTRAINT_NAME=?";
        Map<String, Object> meta = new HashMap<>(4);
        meta.put("OWNER", owner.toUpperCase());
        meta.put("TABLE_NAME", tname.toUpperCase());
        try {
            QueryRunner qr = new QueryRunner(ds);
            List<Map<String, Object>> mapList;
            mapList = qr.query(selectColumns, new MapListHandler(), owner.toUpperCase(), tname.toUpperCase());
            meta.put("COLUMNS", mapList);
            mapList = qr.query(selectConstraintNames, new MapListHandler(), owner.toUpperCase(), tname.toUpperCase());
            List<Map<String, Object>> constraints = new ArrayList<>();
            for (Map<String, Object> m : mapList) {
                Map<String, Object> constraint = new HashMap<>(3);
                String constraintName = m.get("CONSTRAINT_NAME").toString();
                String constraintType = m.get("CONSTRAINT_TYPE").toString();
                mapList = qr.query(selectConstraintColumns, new MapListHandler(), owner.toUpperCase(), tname.toUpperCase(), constraintName);
                constraint.put("constraintName", constraintName);
                constraint.put("constraintType", constraintType);
                constraint.put("constraintColumns", mapList);
                constraints.add(constraint);
            }
            meta.put("CONSTRAINTS", constraints);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return meta;
    }

    public List<Object[]> queryData(String sql, Object[] params) throws SQLException {
        QueryRunner qr = new QueryRunner(ds);
        return qr.query(sql, new ArrayListHandler(), params);
    }
}
