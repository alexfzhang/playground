package alexzhang.tableclone;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Alex F Zhang
 */
public class Clone {
    @SuppressWarnings("unchecked")
    public static void cloneTable() throws SQLException {
        Map<String, Object> conf = ConfUtil.getConf("table_clone_config.json");
        Map<String, Object> db = ConfUtil.getConf("db_info.json");
        assert conf != null;
        assert db != null;
        String frDbinfo = conf.get("fr_db").toString();
        String toDbinfo = conf.get("to_db").toString();
        DbUtil frDb = DbUtil.getDbUtil((Map<String, String>) db.get(frDbinfo));
        DbUtil toDb = DbUtil.getDbUtil((Map<String, String>) db.get(toDbinfo));
        assert frDb != null;
        assert toDb != null;
        Map<String, Object> mappping = ConfUtil.getConf("table_mapping.json");
        assert mappping != null;
        final List<String> include = (List<String>) mappping.get("INCLUDE");
        final List<String> exclude = (List<String>) mappping.get("EXCLUDE");
        List<Map<String, Object>> toClone = null;
        if (include.size() > 0 && exclude.size() == 0) {
            for (String tname : include) {
                List<Map<String, Object>> map = (List<Map<String, Object>>) mappping.get("TBL_MAPPING");
                toClone = map.stream().filter(item -> item.get("fr_tbl_nm").equals(tname)).collect(Collectors.toList());
            }
        } else if (include.size() == 0 && exclude.size() > 0) {
            for (String tname : exclude) {
                List<Map<String, Object>> map = (List<Map<String, Object>>) mappping.get("TBL_MAPPING");
                toClone = map.stream().filter(item -> item.get("fr_tbl_nm").equals(tname)).collect(Collectors.toList());
            }
        } else {
            System.out.println("include and exclude both are not empty.");
        }
        assert toClone != null;
        for (Map<String, Object> tbl : toClone) {
            String frOwner = tbl.get("fr_owner").toString();
            String toOwner = tbl.get("to_owner").toString();
            String frTable = tbl.get("fr_tbl_nm").toString();
            String toTable = tbl.get("to_tbl_nm").toString();
            Map<String, Object> frTableMeta = frDb.getTableMeta(frOwner, frTable);
            Map<String, Object> toTableMeta = toDb.getTableMeta(toOwner, toTable);
            List<Map<String, Object>> frTableColumns = (List<Map<String, Object>>) frTableMeta.get("COLUMNS");
            List<Map<String, Object>> toTableColumns = (List<Map<String, Object>>) toTableMeta.get("COLUMNS");
            if(frTableColumns.equals(toTableColumns)) {
                List<String> params = (List<String>) tbl.get("params");
                String whereClause = tbl.get("where_clause").toString();
                String selectData = String.format("SELECT count(*) FROM %s %s", frTable, whereClause);
                List<Object[]> objects = frDb.queryData(selectData, params.toArray());
                System.out.println(objects);
            }
        }

    }

    public static void main(String[] args) throws Exception {
        cloneTable();
    }
}
