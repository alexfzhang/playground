package alexzhang.tableclone;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * @author Alex F Zhang
 */
public class OraclePool {

    public static DataSource getPool(String propertyFile) throws Exception {
        Properties properties = new Properties();
        InputStream inputStream = DbUtil.class.getClassLoader().getResourceAsStream(propertyFile);
            properties.load(inputStream);
            return DruidDataSourceFactory.createDataSource(properties);
    }

    public static DataSource getPool(Map<String, String> properties) {
        try {
            return DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
