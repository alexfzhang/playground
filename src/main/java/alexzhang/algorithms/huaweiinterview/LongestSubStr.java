package alexzhang.algorithms.huaweiinterview;

import java.util.HashMap;

/**
 * @author Alex Zhang
 */
public class LongestSubStr {
    private int ans;

    public int lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> map = new HashMap<>(s.length());
        int k = -1, ans = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if (map.containsKey(c) && map.get(c) > k) {
                k = map.get(c);
                map.put(c, i);
            } else {
                map.put(c, i);
                ans = Math.max(ans, i - k);
            }
        }
        return ans;
    }

}
