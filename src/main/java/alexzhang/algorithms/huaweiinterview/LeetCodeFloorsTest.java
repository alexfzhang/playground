package alexzhang.algorithms.huaweiinterview;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

/**
 * 某大楼有一部电梯，初始电梯内没有乘客，乘客出现顺序保存在数组 passengers 中，passengers[i]格式为from to，分别表示乘客所在的楼层和想去的楼层。
 * <p>
 * 假设乘客按顺序一个一个出现，当前乘客 passengers[i] 进入电梯且电梯关门后，后一个乘客 passengers[i+1] 立刻出现并等待。
 * <p>
 * 电梯搭载规则如下：
 * <p>
 * ·         当电梯内无乘客时，立即去接当前等待乘客。
 * <p>
 * ·         当电梯内有乘客时：
 * <p>
 * ·         将电梯内乘客都送到之前，电梯不会改变运行方向。
 * <p>
 * ·         若电梯经过当前等待乘客所在楼层、且运行方向与当前等待乘客想去的方向一致，就会顺路搭载当前等待乘客。
 * <p>
 * 假设电梯能携带的乘客数量没有限制，请计算电梯运送完最后一个乘客时，总共运行了多少层的距离。
 * <p>
 * 输入
 * <p>
 * 首行两个整数num initFloor，分别表示乘客的个数和电梯初始所在的楼层，1 <= num <= 100，1 <= initFloor <= 100
 * 接下来 num 行表示passengers信息，passengers[i]格式为from to，分别表示乘客所在的楼层和想去的楼层，1 <= from <= 100，1 <= to <= 100，from != to
 * <p>
 * 输出
 * <p>
 * 一个整数，表示电梯总共运行了多少层的距离
 * <p>
 * 样例
 * <p>
 * 输入样例 1
 * <p>
 * 3 50
 * <p>
 * 12 66
 * <p>
 * 25 27
 * <p>
 * 26 83
 * <p>
 * 输出样例 1
 * <p>
 * 109
 * <p>
 * 输入样例 2
 * <p>
 * 4 50
 * <p>
 * 50 10
 * <p>
 * 40 20
 * <p>
 * 30 100
 * <p>
 * 30 100
 * <p>
 * 输出样例 2
 * <p>
 * 270
 *
 * @author Alex Zhang
 */
public class LeetCodeFloorsTest {
    private int ans = 0;

    private static class Input {
        int num;
        int initFloow;
        int[][] passengers;

        public Input(int num, int initFloow, int[][] passengers) {
            this.num = num;
            this.initFloow = initFloow;
            this.passengers = passengers;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;
        return inputs;
    }

    private void travel(int[][] passengers, List<int[]> passengersInElevator, int direction, int floor) {
        ans += 1;
        for (int i = 1; i < passengers.length; i++) {
            int[] passenger = passengers[i];
            int fr = passenger[0];
            int to = passenger[1];
            int d = (to > fr) ? 1 : -1;
            if (d * direction > 0) {
                passengersInElevator.add(passenger);
            }
            travel(passengers, passengersInElevator, direction, floor + direction);
        }
    }

    public int floors(int num, int initFloow, int[][] passengers) {
        assert (num == passengers.length);
        ans = 0;
        int[] passenger0 = passengers[0];
        int initDirection = (passenger0[1] > passenger0[0]) ? 1 : -1;
        List<int[]> passengersInElevator = new ArrayList<>();
        int floorDiff = Math.abs(initFloow - passenger0[0]);
        ans += floorDiff;
        passengersInElevator.add(passenger0);
        travel(passengers, passengersInElevator, initDirection, initFloow + floorDiff * initDirection);
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
    }
}
