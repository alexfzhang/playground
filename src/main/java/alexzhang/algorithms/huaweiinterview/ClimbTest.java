package alexzhang.algorithms.huaweiinterview;

import org.junit.jupiter.api.Test;

/**
 * @author Alex Zhang
 */
public class ClimbTest {

    public int climbStairs(int n) {
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <=n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    @Test
    public void test() {
        int i = climbStairs(3);
        System.out.println(i);
    }
}
