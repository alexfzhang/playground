package alexzhang.algorithms.dsbysartaj.ch01;

import org.junit.jupiter.api.Test;

/**
 * @author Alex Zhang
 */
public class FactorialTest {
    private int factorial(int n) {
        if (n <= 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
    @Test
    void factorialTest() {
        System.out.println(factorial(5));
    }
}
