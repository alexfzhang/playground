package alexzhang.algorithms.dsbysartaj.ch01;

import org.junit.jupiter.api.Test;

/**
 * 求n个数之和
 *
 * @author Alex Zhang
 */
public class SumTest {
    private int iterateSum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    private int recursiveSum(int[] numbers, int n) {
        if (n > 0) {
            return recursiveSum(numbers, n - 1) + numbers[n - 1];
        }
        return 0;
    }

    @Test
    void iterateSumTest() {
        int s = iterateSum(new int[]{1, 2, 3, 4, 5});
        System.out.println(s);
    }

    @Test
    void recursiveSumTest() {
        int s = recursiveSum(new int[]{1, 2, 3, 4, 5}, 5);
        System.out.println(s);
    }
}
