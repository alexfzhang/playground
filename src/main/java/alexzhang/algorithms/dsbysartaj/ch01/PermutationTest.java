package alexzhang.algorithms.dsbysartaj.ch01;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alex Zhang
 */
public class PermutationTest {
    private <T> List<List<T>> permutations(List<T> elements) {
        List<List<T>> rs = new ArrayList<>();
        permutation(rs, elements, 0, elements.size() - 1);
        return rs;
    }
    private <T> void permutation(List<List<T>> rs, List<T> elements, int k, int m) {
        if(k == m) {
            rs.add(new ArrayList<>(elements));
            return;
        }
        for (int i = k; i <= m; i++) {
            swap(elements, k, i);
            permutation(rs, elements, k + 1, m);
            swap(elements, k, i);
        }
    }
    private <T> void swap(List<T> elements, int a, int b) {
        T c = elements.get(a);
        elements.set(a, elements.get(b));
        elements.set(b, c);
    }
    @Test
    void permutationTest() {
        List<String> input = new ArrayList<>(Arrays.asList("a", "b", "c", "d"));
        List<List<String>> permutations = permutations(input);
        System.out.println(permutations);
    }
}
