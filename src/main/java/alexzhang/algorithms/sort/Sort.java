package alexzhang.algorithms.sort;

import lombok.extern.slf4j.Slf4j;

/**
 * @author alexzhang
 */
@Slf4j
public class Sort {
    private static final int TWO = 2;

    private static <T extends Comparable<T>> int partition(T[] data, int lo, int hi, int order) {
        T pivot = data[hi];
        int i = lo - 1;
        for (int j = lo; j <= hi; j++) {
            if (order == 0) {
                if (data[j].compareTo(pivot) <= 0) {
                    i += 1;
                    T temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            } else {
                if (data[j].compareTo(pivot) >= 0) {
                    i += 1;
                    T temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }
        }
        return i;
    }

    /**
     * sort by order
     *
     * @param data data
     * @param <T>  data item type
     */
    public static <T extends Comparable<T>> void quickSort(T[] data, int low, int high) {
        int order = 0;
        quickSort(data, low, high, order);
    }

    /**
     * sort by order
     *
     * @param data  data
     * @param order order. 0 for ASC, 1 for DESC
     * @param <T>   data item type
     */
    public static <T extends Comparable<T>> void quickSort(T[] data, int low, int high, int order) {
        if (data.length >= TWO) {
            if (low >= 0 && high >= 0 && low < high) {
                int p = partition(data, low, high, order);
                quickSort(data, low, p - 1, order);
                quickSort(data, p + 1, high, order);
            }
        }
    }
}
