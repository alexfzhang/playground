package alexzhang.algorithms.sort;

/**
 * @author alexzhang
 */

public enum SortOrder {

    /**
     * ASC Ascending
     * DESC Descending
     */
    ASC(0), DESC(1);

    SortOrder(int i) {
    }
}
