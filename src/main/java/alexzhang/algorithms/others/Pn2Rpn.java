package alexzhang.algorithms.others;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 * Convert polish notation to reverse polish notation
 * 中缀式转后缀式
 *
 * @author Alex F Zhang
 */
public class Pn2Rpn {
    private static final Map<Character, Integer> PRIORITIES;
    private static final char LEFT_PARENTHESS = '(';
    private static final char RIGHT_PARENTHESS = ')';

    static {
        PRIORITIES = new HashMap<>();
        PRIORITIES.put('+', 1);
        PRIORITIES.put('-', 1);
        PRIORITIES.put('*', 2);
        PRIORITIES.put('/', 2);
    }

    public static String convert(String polishNotation) {
        StringBuilder reversePolishNotation = new StringBuilder();
        Deque<Character> operators = new ArrayDeque<>();
        Character operator;
        for (int i = 0; i < polishNotation.length(); i++) {
            char c = polishNotation.charAt(i);
            if (PRIORITIES.containsKey(c)) {
                int priority = PRIORITIES.getOrDefault(c, 0);
                operator = operators.peek();
                int topPriority = PRIORITIES.getOrDefault(operator, 0);
                while (!operators.isEmpty() && priority <= topPriority) {
                    operator = operators.pop();
                    reversePolishNotation.append(operator);
                    topPriority = PRIORITIES.getOrDefault(operators.peek(), 0);
                }
                operators.push(c);
            } else if (c == LEFT_PARENTHESS) {
                operators.push(c);
            } else if (c == RIGHT_PARENTHESS) {
                operator = operators.pop();
                if (operators.isEmpty()) {
                    if (operator != LEFT_PARENTHESS) {
                        reversePolishNotation.append(operator);
                    }
                } else {
                    while (!operators.isEmpty() && operator != LEFT_PARENTHESS) {
                        reversePolishNotation.append(operator);
                        operator = operators.pop();
                    }
                }
            } else {
                reversePolishNotation.append(c);
            }
        }
        while (!operators.isEmpty()) {
            reversePolishNotation.append(operators.pop());
        }
        return reversePolishNotation.toString();
    }

    public static void main(String[] args) {
        String rpn = convert("a+b*c+(d*e+f)*g");
        // abc*+de*f+g*+
        // abc*+de*f+g*+
        System.out.println(rpn);
    }
}
