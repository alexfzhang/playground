package alexzhang.algorithms.lagoualgorithms.backtracking;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * * @author Alex F Zhang
 */
public class MaxString {

    private void solve(List<String> arr) {
        int max = 0;

    }

    private int dfs(int[] bitmap, int[] lens, int start, int num) {
        if(start >= bitmap.length) {
            return 0;
        }
        int ans = 0;
        for (int i = start; i < bitmap.length; i++) {
            if(bitmap[i] != -1 && (bitmap[i] & num) == 0) {
                ans = Math.max(ans, dfs(bitmap, lens, i + 1, num | bitmap[i])+ lens[i]);
            }
        }
        return ans;
    }

    private boolean isStringUnique(String s) {
        Set<Character> exists = new HashSet<>();
        for(char c: s.toCharArray()) {
            if(exists.contains(c)) {
                return false;
            } else {
                exists.add(c);
            }
        }
        return true;
    }

    private int getBitMask(String s) {
        int bitMask = 0;
        for(char c: s.toCharArray()) {
            System.out.println((c - 'a'));
            int bit = 1 << (c - 'a');
            if((bitMask & bit) != 0) {
                return -1;
            }
            bitMask |= bit;
        }
        return bitMask;
    }

    public int maxLength(List<String> arr) {
        int[] bitmap = new int[arr.size()];
        int[] lens = new int[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            String s = arr.get(i);
            bitmap[i] = getBitMask(s);
            lens[i] = s.length();
        }
        return dfs(bitmap, lens, 0, 0);
    }

}
