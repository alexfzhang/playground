package alexzhang.algorithms.lagoualgorithms.backtracking;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 设计一种算法，打印N皇后在N x N棋盘上的各种摆法，其中每个皇后都不同行，不同列，也不在斜线上
 *
 * @author Alex F Zhang
 */
public class EightQueens {

    private final Set<Integer> columns = new HashSet<>();
    private final Set<Integer> leftDiagonal = new HashSet<>();
    private final Set<Integer> rightDiagonal = new HashSet<>();
    private int[] queensIdxInRow;

    /**
     * 暴力解法，用N x N矩阵保存状态
     */
    private List<List<String>> solution1(int n) {
        char[][] board = new char[n][n];
        for (char[] c : board) {
            Arrays.fill(c, '.');
        }
        List<List<String>> rs = new ArrayList<>();
        solve1(rs, board, 0);
        return rs;
    }

    private boolean validate1(char[][] board, int row, int column) {
        for (int i = row - 1; i >= 0; i--) {
            if (board[i][column] == 'Q') {
                return false;
            }
        }
        for (int i = row - 1, j = column + 1; i >= 0 && j < board.length; i--, j++) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }
        for (int i = row - 1, j = column - 1; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }

    private void solve1(List<List<String>> rs, char[][] board, int row) {
        if (row == board.length) {
            rs.add(construct(board));
            return;
        }
        for (int col = 0; col < board.length; col++) {
            if (validate1(board, row, col)) {
                char[][] temp = copy(board);
                temp[row][col] = 'Q';
                solve1(rs, temp, row + 1);
            }
        }
    }

    private char[][] copy(char[][] src) {
        char[][] dest = new char[src.length][src.length];
        for (int i = 0; i < src.length; i++) {
            System.arraycopy(src[i], 0, dest[i], 0, src.length);
        }
        return dest;
    }

    private List<String> construct(char[][] board) {
        List<String> line = new ArrayList<>();
        for (char[] ca : board) {
            StringBuilder s = new StringBuilder();
            for (char c : ca) {
                s.append(c);
            }
            line.add(s.toString());
        }
        return line;
    }

    /**
     * 空间优化解法
     */
    private List<List<String>> solution2(int n) {
        queensIdxInRow = new int[n];
        Arrays.fill(queensIdxInRow, -1);
        List<List<String>> rs = new ArrayList<>();
        solve(rs, n, 0);
        return rs;
    }
    private void solve(List<List<String>> solutions, int n, int row) {
        if(row == n) {
            solutions.add(generateBorad(n));
            return;
        }
        for (int col = 0; col < n; col++) {
            if(!columns.contains(col) && !leftDiagonal.contains(row - col) && !rightDiagonal.contains(row + col)) {
                columns.add(col);
                leftDiagonal.add(row - col);
                rightDiagonal.add(row + col);
                queensIdxInRow[row] = col;
                solve(solutions, n, row + 1);
                queensIdxInRow[row] = -1;
                columns.remove(col);
                leftDiagonal.remove(row - col);
                rightDiagonal.remove(row + col);
            }
        }
    }

    private List<String> generateBorad(int n) {
        List<String> s = new ArrayList<>();
        for(int idx: queensIdxInRow) {
            char[] r = new char[n];
            Arrays.fill(r, '.');
            r[idx] = 'Q';
            s.add(new String(r));
        }
        return s;
    }

    /**
     * 定义输入
     */
    private static class Input {
        int n;

        public Input(int n) {
            this.n = n;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(4),
                new Input(8)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void eightQueensTest(Input input) {
        int n = input.n;
        List<List<String>> solution = solution1(n);
        System.out.println("===================================================================");
        for (List<String> l : solution) {
            for (String s : l) {
                System.out.println(s);
            }
            System.out.println();
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void eightQueensTest2(Input input) {
        int n = input.n;
        List<List<String>> solution = solution2(n);
        System.out.println("===================================================================");
        for (List<String> l : solution) {
            for (String s : l) {
                System.out.println(s);
            }
            System.out.println();
        }
    }
}
