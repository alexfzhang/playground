package alexzhang.algorithms.datastructure;

import java.util.*;

/**
 * @author Alex F Zhang
 */
public class TreeNodeTraversal {
    public static List<Integer> preorderTraversalNonRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (null != root) {
            Deque<TreeNode> s = new ArrayDeque<>();
            TreeNode node = root;
            while (null != node) {
                while (null != node) {
                    ans.add(node.val);
                    s.push(node);
                    node = node.left;
                }
                while (null == node && !s.isEmpty()) {
                    node = s.pop();
                    node = node.right;
                }
            }
        }
        return ans;
    }

    private static void preorderTraversalRecursive(List<Integer> ans, TreeNode root) {
        if (null == root) {
            return;
        }
        ans.add(root.val);
        preorderTraversalRecursive(ans, root.left);
        preorderTraversalRecursive(ans, root.right);
    }

    public static List<Integer> preorderTraversalRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        preorderTraversalRecursive(ans, root);
        return ans;
    }

    public static List<Integer> inorderTraversalNonRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (null != root) {
            Deque<TreeNode> s = new ArrayDeque<>();
            TreeNode node = root;
            while (null != node) {
                while (null != node) {
                    s.push(node);
                    node = node.left;
                }
                while (null == node && !s.isEmpty()) {
                    node = s.pop();
                    ans.add(node.val);
                    node = node.right;
                }
            }
        }
        return ans;
    }

    private static void inorderTraversalRecursive(List<Integer> ans, TreeNode root) {
        if (null == root) {
            return;
        }
        inorderTraversalRecursive(ans, root.left);
        ans.add(root.val);
        inorderTraversalRecursive(ans, root.right);
    }

    public static List<Integer> inorderTraversalRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        inorderTraversalRecursive(ans, root);
        return ans;
    }

    public static List<Integer> postorderTraversalNonRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (null != root) {
            Deque<TreeNode> s = new ArrayDeque<>();
            s.push(root);
            while (!s.isEmpty()) {
                TreeNode curr = s.pop();
                if (null != curr.left) {
                    s.push(curr.left);
                }
                if (null != curr.right) {
                    s.push(curr.right);
                }
                ans.add(curr.val);
            }
        }
        Collections.reverse(ans);
        return ans;
    }

    public static List<Integer> postorderTraversalNonRecursive2(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        if (null != root) {
            Deque<TreeNode> s = new ArrayDeque<>();
            TreeNode node = root;
            TreeNode last = null;
            while (!s.isEmpty() || null != node) {
                while (null != node) {
                    s.push(node);
                    node = node.left;
                }
                TreeNode peek = s.peek();
                if (null != peek) {
                    if (null == peek.right || peek.right == last) {
                        peek = s.pop();
                        ans.add(peek.val);
                        last = peek;
                        node = null;
                    } else {
                        node = peek.right;
                    }
                }
            }
        }
        return ans;
    }

    private static void postorderTraversalRecursive(List<Integer> ans, TreeNode root) {
        if (null == root) {
            return;
        }
        postorderTraversalRecursive(ans, root.left);
        postorderTraversalRecursive(ans, root.right);
        ans.add(root.val);
    }

    public static List<Integer> postorderTraversalRecursive(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        postorderTraversalRecursive(ans, root);
        return ans;
    }
}
