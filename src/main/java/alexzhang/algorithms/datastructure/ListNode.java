package alexzhang.algorithms.datastructure;

import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * @author alexzhang
 */
@Slf4j
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int... values) {
        ListNode h;
        if(values.length > 0) {
            h = new ListNode(values[0]);
            ListNode p = h;
            for(int i = 1;i<values.length;i++) {
                p.next = new ListNode(values[i]);
                p = p.next;
            }
        }
    }

    public static ListNode create(int... numbers) {
        ListNode h;
        if (numbers.length > 0) {
            h = new ListNode(numbers[0]);
            ListNode p = h;
            for (int i = 1; i < numbers.length; i++) {
                p.next = new ListNode(numbers[i]);
                p = p.next;
            }
        } else {
            h = null;
        }
        return h;
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ListNode listNode = (ListNode) o;
        ListNode b = this;
        while (null != b && null != listNode) {
            if (b.val != listNode.val) {
                return false;
            }
            b = b.next;
            listNode = listNode.next;
        }
        return null == b && null == listNode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(val, next);
    }

    @Override
    public String toString() {
        ListNode a = this;
        StringBuilder s = new StringBuilder();
        while (null != a) {
            s.append(a.val).append("->");
            a = a.next;
        }
        s.setLength(s.length() - 2);
        return s.toString();
    }
}
