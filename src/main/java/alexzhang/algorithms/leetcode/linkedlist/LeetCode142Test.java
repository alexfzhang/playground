package alexzhang.algorithms.leetcode.linkedlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * 给定一个链表，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。
 * <p>
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。 为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到
 * 链表中的位置（索引从 0 开始）。如果 pos 是 -1，则在该链表中没有环。注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
 * <p>
 * 不允许修改 链表。
 * <p>
 * 示例 1：
 * <p>
 * 输入：head = [3,2,0,-4], pos = 1
 * <p>
 * 输出：返回索引为 1 的链表节点
 * <p>
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 * <p>
 * 示例 2：
 * <p>
 * 输入：head = [1,2], pos = 0
 * <p>
 * 输出：返回索引为 0 的链表节点
 * <p>
 * 解释：链表中有一个环，其尾部连接到第一个节点。
 * <p>
 * 示例 3：
 * <p>
 * 输入：head = [1], pos = -1
 * <p>
 * 输出：返回 null
 * <p>
 * 解释：链表中没有环。
 * <p>
 * 提示：
 * <p>
 * 链表中节点的数目范围在范围 [0, 10⁴] 内
 * <p>
 * -10⁵ <= Node.val <= 10⁵
 * <p>
 * pos 的值为 -1 或者链表中的一个有效索引
 * <p>
 * 进阶：你是否可以使用 O(1) 空间解决此题？
 * <p>
 * Related Topics 哈希表 链表 双指针 👍 1249 👎 0
 *
 * @author alexzhang
 */
public class LeetCode142Test {

    private static class ListNode {
        int val;
        ListNode next;
        static int length = 0;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                length = numbers.length;
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            int i = length;
            while (null != a && i > 0) {
                s.append(a.val).append(",");
                a = a.next;
                i -= 1;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode head;
        private final ListNode expected;

        public Input(ListNode head, ListNode expected) {
            this.head = head;
            this.expected = expected;
        }

        public ListNode getHead() {
            return head;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static ListNode getTail(ListNode head) {
        ListNode p = head;
        while (null != head && null != p.next) {
            p = p.next;
        }
        return p;
    }

    private static Input[] createInput() {
        ListNode c1 = ListNode.create(1, 2, 3, 4, 5, 6, 7, 8, 9);
        ListNode a = c1.next;
        System.out.println(a.val);
        ListNode b = getTail(c1);
        b.next = a;
        System.out.println("createInput: " + "a: " + a.val + ", b: " + b.val + ", b.next: " + b.next.val);
        return new Input[]{
                new Input(c1, a)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void detectCycle(Input input) {
        ListNode head = input.getHead();
        ListNode expected = input.getExpected();
        ListNode ptr = null;
        if (null != head) {
            ListNode s = head;
            ListNode f = head;
            while (null != f) {
                s = s.next;
                if (null != f.next) {
                    f = f.next.next;
                } else {
                    break;
                }
                if (s == f) {
                    ptr = head;
                    while (ptr != s) {
                        ptr = ptr.next;
                        s = s.next;
                    }
                    break;
                }
            }
        }
        Assertions.assertSame(expected, ptr);
    }
}
