package alexzhang.algorithms.leetcode.linkedlist;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Objects;

/**
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 * <p>
 * 示例 1：
 * <p>
 * 输入：l1 = [1,2,4], l2 = [1,3,4]
 * <p>
 * 输出：[1,1,2,3,4,4]
 * <p>
 * 示例 2：
 * <p>
 * 输入：l1 = [], l2 = []
 * <p>
 * 输出：[]
 * <p>
 * 示例 3：
 * <p>
 * 输入：l1 = [], l2 = [0]
 * <p>
 * 输出：[0]
 * <p>
 * 提示：
 * <p>
 * 两个链表的节点数目范围是 [0, 50]
 * <p>
 * -100 <= Node.val <= 100
 * <p>
 * l1 和 l2 均按 非递减顺序 排列
 * <p>
 * Related Topics 递归 链表 👍 2012 👎 0
 *
 * @author alexzhang
 */
public class LeetCode21Test {
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l1;
        private final ListNode l2;
        private final ListNode expected;

        public Input(ListNode l1, ListNode l2, ListNode expected) {
            this.l1 = l1;
            this.l2 = l2;
            this.expected = expected;
        }

        public ListNode getL1() {
            return l1;
        }

        public ListNode getL2() {
            return l2;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(2), ListNode.create(1), ListNode.create(1, 2)),
                new Input(ListNode.create(2), ListNode.create(3), ListNode.create(2, 3)),
                new Input(ListNode.create(2, 3, 4), ListNode.create(1, 2, 4), ListNode.create(1, 2, 2, 3, 4, 4)),
                new Input(ListNode.create(2, 3, 4, 6, 7), ListNode.create(1, 2, 4), ListNode.create(1, 2, 2, 3, 4, 4, 6, 7))
//                new Input(ListNode.create(1), ListNode.create(), ListNode.create(1)),
//                new Input(ListNode.create(), ListNode.create(2), ListNode.create(2)),
//                new Input(ListNode.create(1), ListNode.create(2), ListNode.create(1, 2)),
//                new Input(ListNode.create(), ListNode.create(), ListNode.create())
        };
    }

    /**
     * insert both into a new list
     *
     * @param input input
     */
    @ParameterizedTest
    @MethodSource("createInput")
    public void mergeTwoLists(Input input) {
        ListNode h1 = input.getL1();
        ListNode h2 = input.getL2();
        if (null == h1 && null != h2) {
            assertEquals(input.getExpected(), h2);
        } else if (null != h1 && null == h2) {
            assertEquals(input.getExpected(), h1);
        } else if (null == h1) {
            assertNull(input.getExpected());
        } else {
            ListNode a = h1;
            ListNode b = h2;
            ListNode rs = new ListNode();
            if (a.val > b.val) {
                rs.val = b.val;
                b = b.next;
            } else {
                rs.val = a.val;
                a = a.next;
            }
            ListNode p = rs;
            while (null != a && null != b) {
                if (a.val > b.val) {
                    p.next = b;
                    b = b.next;
                } else {
                    p.next = a;
                    a = a.next;
                }
                p = p.next;
            }
            while (null != a) {
                p.next = a;
                p = p.next;
                a = a.next;
            }
            while (null != b) {
                p.next = b;
                p = p.next;
                b = b.next;
            }
            assertEquals(input.getExpected(), rs);
        }
    }

    /**
     * inplace merge
     *
     * @param input input
     */
    @ParameterizedTest
    @MethodSource("createInput")
    public void inplaceMergeTwoLists(Input input) {
        ListNode l1 = input.getL1();
        ListNode l2 = input.getL2();
        ListNode expected = input.getExpected();
        if (null == l1 && null == l2) {
            assertNull(expected);
        } else if (null == l1) {
            assertEquals(expected, l2);
        } else if (null == l2) {
            assertEquals(expected, l1);
        } else {
            ListNode p1 = l1;
            ListNode p2 = l2;
            ListNode p;
            if (p1.val > p2.val) {
                // merge to l2
                p = p2;
                while (null != p1) {
                    while (null != p2 && p1.val > p2.val) {
                        p = p2;
                        p2 = p2.next;
                    }
                    p.next = p1;
                    p1 = p1.next;
                    p = p.next;
                    p.next = p2;
                }
                assertEquals(expected, l2);
            } else {
                // merge to l1
                p = p1;
                while (null != p2) {
                    while (null != p1 && p1.val <= p2.val) {
                        p = p1;
                        p1 = p1.next;
                    }
                    p.next = p2;
                    p2 = p2.next;
                    p = p.next;
                    p.next = p1;
                }
                assertEquals(expected, l1);
            }
        }
    }
}
