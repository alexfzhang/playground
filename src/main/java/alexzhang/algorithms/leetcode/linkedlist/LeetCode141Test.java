package alexzhang.algorithms.leetcode.linkedlist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * 给你一个链表的头节点 head ，判断链表中是否有环。
 * <p>
 * 如果链表中有某个节点，可以通过连续跟踪 next 指针再次到达，则链表中存在环。 为了表示给定链表中的环，评测系统内部使用整数 pos 来表示链表尾连接到
 * 链表中的位置（索引从 0 开始）。如果 pos 是 -1，则在该链表中没有环。注意：pos 不作为参数进行传递，仅仅是为了标识链表的实际情况。
 * <p>
 * 如果链表中存在环，则返回 true 。 否则，返回 false 。
 * <p>
 * 示例 1：
 * <p>
 * 输入：head = [3,2,0,-4], pos = 1
 * <p>
 * 输出：true
 * <p>
 * 解释：链表中有一个环，其尾部连接到第二个节点。
 * <p>
 * 示例 2：
 * <p>
 * 输入：head = [1,2], pos = 0
 * <p>
 * 输出：true
 * <p>
 * 解释：链表中有一个环，其尾部连接到第一个节点。
 * <p>
 * 示例 3：
 * <p>
 * 输入：head = [1], pos = -1
 * <p>
 * 输出：false
 * <p>
 * 解释：链表中没有环。
 * <p>
 * 提示：
 * <p>
 * 链表中节点的数目范围是 [0, 10⁴]
 * <p>
 * -10⁵ <= Node.val <= 10⁵
 * <p>
 * pos 为 -1 或者链表中的一个 有效索引 。
 * <p>
 * 进阶：你能用 O(1)（即，常量）内存解决此问题吗？
 * <p>
 * Related Topics 哈希表 链表 双指针 👍 1252 👎 0
 *
 * @author alexzhang
 */
public class LeetCode141Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append("->");
                a = a.next;
            }
            s.setLength(s.length() - 2);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode head;
        private final boolean expected;

        public Input(ListNode head, boolean expected) {
            this.head = head;
            this.expected = expected;
        }

        public ListNode getHead() {
            return head;
        }

        public boolean getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        ListNode c1 = ListNode.create(3, 2, 0, -4);
        ListNode a = c1.next;
        ListNode b = c1.next.next.next;
        b.next = a;
        ListNode c2 = ListNode.create(1, 2);
        a = c2;
        b = c2.next;
        b.next = a;
        ListNode c3 = ListNode.create(1);
        a = c3;
        b = c3;
        b.next = a;
        return new Input[]{
                new Input(ListNode.create(), false),
                new Input(ListNode.create(1, 2, 3, 4), false),
                new Input(ListNode.create(1), false),
                new Input(c1, true),
                new Input(c2, true),
                new Input(c3, true)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void hasCycle(Input input) {
        final ListNode head = input.getHead();
        final boolean expected = input.getExpected();
        boolean rs = false;
        if (null != head) {
            ListNode slow = head;
            ListNode fast = head.next;
            while (null != slow && null != fast && null != fast.next) {
                if (slow == fast) {
                    rs = true;
                    break;
                }
                slow = slow.next;
                fast = fast.next.next;
            }
        }
        Assertions.assertEquals(expected, rs);
    }
}
