package alexzhang.algorithms.leetcode.heap;

import alexzhang.algorithms.leetcode.util.CaseReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给定两个以 升序排列 的整数数组 nums1 和 nums2 , 以及一个整数 k 。
 * <p>
 * 定义一对值 (u,v)，其中第一个元素来自 nums1，第二个元素来自 nums2 。
 * <p>
 * 请找到和最小的 k 个数对 (u1,v1),  (u2,v2) ... (uk,vk) 。
 * <p>
 * 示例 1:
 * <p>
 * 输入: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
 * 输出: [1,2],[1,4],[1,6]
 * <p>
 * 解释: 返回序列中的前 3 对数：
 * <p>
 * [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]
 * <p>
 * 示例 2:
 * <p>
 * 输入: nums1 = [1,1,2], nums2 = [1,2,3], k = 2
 * <p>
 * 输出: [1,1],[1,1]
 * <p>
 * 解释: 返回序列中的前 2 对数：
 * <p>
 * [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]
 * <p>
 * 示例 3:
 * <p>
 * 输入: nums1 = [1,2], nums2 = [3], k = 3
 * <p>
 * 输出: [1,3],[2,3]
 * <p>
 * 解释: 也可能序列中所有的数对都被返回:[1,3],[2,3]
 * <p>
 * 提示:
 * <p>
 * 1 <= nums1.length, nums2.length <= 105
 * <p>
 * -109 <= nums1[i], nums2[i] <= 109
 * <p>
 * nums1 和 nums2 均为升序排列
 * <p>
 * 1 <= k <= 1000
 * <p>
 * Related Topics
 * <p>
 * 数组
 * <p>
 * 堆（优先队列）
 * <p>
 * 👍 297
 * <p>
 * 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode373Test {

    private static class ValuePair implements Comparable<ValuePair> {
        private final List<Integer> values;
        private final int sum;

        public ValuePair(int v1, int v2) {
            values = Arrays.asList(v1, v2);
            this.sum = v1 + v2;
        }

        @Override
        public int compareTo(ValuePair vp2) {
            return this.sum - vp2.sum;
        }
    }

    private static class Input {
        int[] nums1;
        int[] nums2;
        int k;
        List<List<Integer>> expected;

        public Input(int[] nums1, int[] nums2, int k, List<List<Integer>> expected) {
            this.nums1 = nums1;
            this.nums2 = nums2;
            this.k = k;
            this.expected = expected;
        }
    }

    @SuppressWarnings("unchecked")
    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        List<Map<String, Object>> rawInput = (List<Map<String, Object>>) CaseReader.read("leetcode_cases/leetcode373_cases.json");
        if (null != rawInput) {
            for (Map<String, Object> cs : rawInput) {
                List<Integer> nums1List = (List<Integer>) cs.get("nums1");
                int[] nums1 = new int[nums1List.size()];
                for (int i = 0; i < nums1List.size(); i++) {
                    nums1[i] = nums1List.get(i);
                }
                List<Integer> nums2List = (List<Integer>) cs.get("nums2");
                int[] nums2 = new int[nums2List.size()];
                for (int i = 0; i < nums2List.size(); i++) {
                    nums2[i] = nums2List.get(i);
                }
                input = new Input(nums1, nums2, (int) cs.get("k"), (List<List<Integer>>) cs.get("expected"));
                inputs.add(input);
            }
        }

        return inputs;
    }

    public List<List<Integer>> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        PriorityQueue<ValuePair> pq = new PriorityQueue<>();
        for (int value : nums1) {
            for (int i : nums2) {
                pq.add(new ValuePair(value, i));
            }
        }
        List<List<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < k; i++) {
            ValuePair vp = pq.poll();
            if (null != vp) {
                ans.add(vp.values);
            }
        }
        return ans;
    }

    public List<List<Integer>> kSmallestPairs2(int[] nums1, int[] nums2, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(k, Comparator.comparingInt(o -> nums1[o[0]] + nums2[o[1]]));
        List<List<Integer>> ans = new ArrayList<>();
        int len1 = nums1.length;
        int len2 = nums2.length;
        for (int i = 0; i < Math.min(len1, k); i++) {
            pq.add(new int[]{i, 0});
        }
        while (k > 0 && !pq.isEmpty()) {
            int[] idxPair = pq.poll();
            List<Integer> list = new ArrayList<>();
            list.add(nums1[idxPair[0]]);
            list.add(nums2[idxPair[1]]);
            ans.add(list);
            if (idxPair[1] + 1 < len2) {
                pq.add(new int[]{idxPair[0], idxPair[1] + 1});
            }
            k -= 1;
        }
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] nums1 = input.nums1;
        int[] nums2 = input.nums2;
        int k = input.k;
        List<List<Integer>> expected = input.expected;
        List<List<Integer>> ans = kSmallestPairs2(nums1, nums2, k);
        Assertions.assertEquals(expected, ans);
    }

}
