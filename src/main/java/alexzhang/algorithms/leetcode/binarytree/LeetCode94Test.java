package alexzhang.algorithms.leetcode.binarytree;

import alexzhang.algorithms.datastructure.TreeNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * @author Alex F Zhang
 */
public class LeetCode94Test {
    private static class Input {
        TreeNode root;
        List<Integer> expected;

        public Input(TreeNode root, List<Integer> expected) {
            this.root = root;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;
        TreeNode root;

        root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);
        input = new Input(root, Arrays.asList(1, 3, 2));
        inputs.add(input);

        return inputs;
    }

    private void traversal(List<Integer> ans, TreeNode root) {
        if (null == root) {
            return;
        }
        traversal(ans, root.left);
        ans.add(root.val);
        traversal(ans, root.right);
    }

    private void travaeralNonRecursive(List<Integer> ans, TreeNode root) {
        if (null == root) {
            return;
        }
        Deque<TreeNode> s = new ArrayDeque<>();
        TreeNode node = root;
        while (null != node || !s.isEmpty()) {
            while (null != node) {
                s.push(node);
                node = node.left;
            }
            if (!s.isEmpty()) {
                node = s.pop();
                ans.add(node.val);
                node = node.right;
            }
        }
    }

    private List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
//        traversal(ans, root);
        travaeralNonRecursive(ans, root);
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        TreeNode root = input.root;
        List<Integer> expected = input.expected;
        List<Integer> ans = inorderTraversal(root);
        Assertions.assertEquals(expected, ans);
    }
}
