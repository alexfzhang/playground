package alexzhang.algorithms.leetcode.binarytree;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.ArrayList;

/**
 * 给你一个整数 n ，求恰由 n 个节点组成且节点值从 1 到 n 互不相同的 二叉搜索树 有多少种？返回满足题意的二叉搜索树的种数。
 * <p>
 * 示例 1：
 * <img src="https://assets.leetcode.com/uploads/2021/01/18/uniquebstn3.jpg"/>
 * <p>
 * 输入：n = 3
 * <p>
 * 输出：5
 * <p>
 * 示例 2：
 * <p>
 * 输入：n = 1
 * <p>
 * 输出：1
 * <p>
 * 提示：
 * <p>
 * 1 <= n <= 19
 * <p>
 * Related Topics 树 二叉搜索树 数学 动态规划 二叉树 👍 1440 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode96Test {
    private boolean[] visited;
    private int count;

    private static class Input {
        int n;
        int expected;

        public Input(int n, int expected) {
            this.n = n;
            this.expected = expected;
        }
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode() {
        }

        public TreeNode(int val) {
            this.val = val;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(3, 5);
        inputs.add(input);

        input = new Input(1, 1);
        inputs.add(input);

        return inputs;
    }

    private void solve(int n, int k) {
        if (k > n) {
            count += 1;
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (!visited[i - 1]) {
                visited[i - 1] = true;
                solve(n, k + 1);
                visited[i - 1] = false;
            }
        }
    }

    public int numTrees(int n) {
        visited = new boolean[n];
        count = 0;
        solve(n, 1);
        return count;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int n = input.n;
        int expected = input.expected;
        int ans = numTrees(n);
        Assertions.assertEquals(expected, ans);
    }
}
