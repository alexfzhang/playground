package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给定一个数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
 * <p>
 * candidates 中的每个数字在每个组合中只能使用一次。
 * <p>
 * 注意：解集不能包含重复的组合。
 * <p>
 * 示例 1:
 * <p>
 * 输入: candidates = [10,1,2,7,6,1,5], target = 8,
 * <p>
 * 输出:
 * <p>
 * [
 * <p>
 * [1,1,6],
 * <p>
 * [1,2,5],
 * <p>
 * [1,7],
 * <p>
 * [2,6]
 * <p>
 * ]
 * <p>
 * 示例 2:
 * <p>
 * 输入: candidates = [2,5,2,1,2], target = 5,
 * <p>
 * 输出:
 * <p>
 * [
 * <p>
 * [1,2,2],
 * <p>
 * [5]
 * <p>
 * ]
 * <p>
 * 提示:
 * <p>
 * 1 <= candidates.length <= 100
 * <p>
 * 1 <= candidates[i] <= 50
 * <p>
 * 1 <= target <= 30
 * <p>
 * Related Topics 数组 回溯 👍 717 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode40Test {
    List<List<Integer>> ans;
    Deque<Integer> combination;
    boolean[] visited;

    private static class Input {
        int[] candidates;
        int target;
        List<List<Integer>> expected;

        public Input(int[] candidates, int target, List<List<Integer>> expected) {
            this.candidates = candidates;
            this.target = target;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        List<List<Integer>> expected;
        Input input;

        expected = new ArrayList<>();
        expected.add(Arrays.asList(1, 1, 6));
        expected.add(Arrays.asList(1, 2, 5));
        expected.add(Arrays.asList(1, 7));
        expected.add(Arrays.asList(2, 6));
        input = new Input(new int[]{10, 1, 2, 7, 6, 1, 5}, 8, expected);
        inputs.add(input);

        expected = new ArrayList<>();
        expected.add(Arrays.asList(1, 2, 2));
        expected.add(Collections.singletonList(5));
        input = new Input(new int[]{2, 5, 2, 1, 2}, 5, expected);
        inputs.add(input);

        return inputs;
    }

    private void solution1(int[] candidates, int target, int idx) {
        if (target == 0) {
            ans.add(new ArrayList<>(combination));
            return;
        }
        for (int i = idx; i < candidates.length; i++) {
            if (target - candidates[i] >= 0) {
                if (i == idx || candidates[i] != candidates[i - 1]) {
                    combination.addLast(candidates[i]);
                    solution1(candidates, target - candidates[i], i + 1);
                    combination.removeLast();
                }
            }
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] candidates = input.candidates;
        int target = input.target;
        List<List<Integer>> expected = input.expected;
        ans = new ArrayList<>();
        combination = new ArrayDeque<>();
        Arrays.sort(candidates);
        solution1(candidates, target, 0);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }
}
