package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。
 * <p>
 * 你可以按 任何顺序 返回答案。
 * <p>
 * 示例 1：
 * <p>
 * 输入：n = 4, k = 2
 * <p>
 * 输出：
 * <p>
 * [
 * <p>
 * [2,4],
 * <p>
 * [3,4],
 * <p>
 * [2,3],
 * <p>
 * [1,2],
 * <p>
 * [1,3],
 * <p>
 * [1,4],
 * <p>
 * ]
 * <p>
 * 示例 2：
 * <p>
 * 输入：n = 1, k = 1
 * <p>
 * 输出：[[1]]
 * <p>
 * 提示：
 * <p>
 * 1 <= n <= 20
 * <p>
 * 1 <= k <= n
 * <p>
 * Related Topics 数组 回溯 👍 743 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode77Test {

    private static class Input {
        int n;
        int k;
        List<List<Integer>> expected;

        public Input(int n, int k, List<List<Integer>> expected) {
            this.n = n;
            this.k = k;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        List<List<Integer>> e = new ArrayList<>();
        Input input;
        e.add(Arrays.asList(1, 2));
        e.add(Arrays.asList(1, 3));
        e.add(Arrays.asList(1, 4));
        e.add(Arrays.asList(2, 3));
        e.add(Arrays.asList(2, 4));
        e.add(Arrays.asList(3, 4));
        input = new Input(4, 2, e);
        inputs.add(input);

        e = new ArrayList<>();
        e.add(Collections.singletonList(1));
        input = new Input(1, 1, e);
        inputs.add(input);

        e = new ArrayList<>();
        e.add(Arrays.asList(1, 2, 3, 4));
        input = new Input(4, 4, e);
        inputs.add(input);

        return inputs;
    }

    private void combinations(List<List<Integer>> ans, List<Integer> combination, int n, int k, int a, int b) {
        if (a == k) {
            ans.add(new ArrayList<>(combination));
            return;
        }
        for (int i = b; i < n; i++) {
            combination.add(i + 1);
            combinations(ans, combination, n, k, a + 1, i + 1);
            combination.remove(a);
        }
    }

    private void combinations2(List<List<Integer>> ans, List<Integer> combination, int n, int k, int m) {
        if ((combination.size() + (n - m + 1)) < k) {
            return;
        }
        if (combination.size() == k) {
            ans.add(new ArrayList<>(combination));
            return;
        }
        combination.add(m);
        combinations2(ans, combination, n, k, m + 1);
        combination.remove(combination.size() - 1);
        combinations2(ans, combination, n, k, m + 1);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void combine(Input input) {
        int n = input.n;
        int k = input.k;
        List<List<Integer>> expected = input.expected;
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> combination = new ArrayList<>();
        combinations(ans, combination, n, k, 0, 0);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void combine2(Input input) {
        int n = input.n;
        int k = input.k;
        List<List<Integer>> expected = input.expected;
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> combination = new ArrayList<>();
        combinations2(ans, combination, n, k, 1);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }
}
