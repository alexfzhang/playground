package alexzhang.algorithms.leetcode.backtrace;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * n 皇后问题 研究的是如何将 n 个皇后放置在 n×n 的棋盘上，并且使皇后彼此之间不能相互攻击。
 * <p>
 * 给你一个整数 n ，返回 n 皇后问题 不同的解决方案的数量。
 * <p>
 * 示例 1：
 * 输入：n = 4
 * 输出：2
 * 解释：如上图所示，4 皇后问题存在两个不同的解法。
 * <p>
 * 示例 2：
 * 输入：n = 1
 * 输出：1
 * <p>
 * 提示：
 * 1 <= n <= 9
 * 皇后彼此不能相互攻击，也就是说：任何两个皇后都不能处于同一条横行、纵行或斜线上。
 * <p>
 * Related Topics 回溯 👍 290 👎 0
 *
 * @author Alex F Zhang
 */

public class LeetCode52Test {
    private final Set<Integer> column = new HashSet<>();
    /**
     * top-left to bottom-rifht, (row - col) in [-7, 7]
     */
    private final Set<Integer> leftDiagonal = new HashSet<>();
    /**
     * top-right to bottom-left, (row + col) in [0, 14]
     */
    private final Set<Integer> rightDiagonal = new HashSet<>();
    private int count = 0;

    private int solution(int n) {
        solve(n, 0);
        return count;
    }

    private void solve(int n, int row) {
        if (row == n) {
            count += 1;
            return;
        }
        for (int col = 0; col < n; col++) {
            if(!column.contains(col) && !leftDiagonal.contains(row - col) && !rightDiagonal.contains(row + col)) {
                column.add(col);
                leftDiagonal.add(row - col);
                rightDiagonal.add(row + col);
                solve(n, row + 1);
                column.remove(col);
                leftDiagonal.remove(row - col);
                rightDiagonal.remove(row + col);
            }
        }
    }

    private static class Input {
        int n;
        int expected;

        public Input(int n, int expected) {
            this.n = n;
            this.expected = expected;
        }

    }

    private static Input[] createInput() {
        try (InputStream resourceAsStream = LeetCode52Test.class.getClassLoader().getResourceAsStream("leetcode_cases/leetcode52_cases.json")) {
            if (null != resourceAsStream) {
                String confStr = IOUtils.toString(resourceAsStream);
                Map<String, Object> map = new JSONObject(confStr).toMap();
                return new Input[]{
                        new Input(1, (int) map.get("1")),
                        new Input(2, (int) map.get("2")),
                        new Input(3, (int) map.get("3")),
                        new Input(4, (int) map.get("4")),
                        new Input(5, (int) map.get("5")),
                        new Input(6, (int) map.get("6")),
                        new Input(7, (int) map.get("7")),
                        new Input(8, (int) map.get("8")),
                        new Input(9, (int) map.get("9"))
                };
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new Input[]{
                new Input(1, 0),
                new Input(2, 0),
                new Input(3, 0),
                new Input(4, 0),
                new Input(5, 0),
                new Input(6, 0),
                new Input(7, 0),
                new Input(8, 0),
                new Input(9, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void eightQueensTest(Input input) {
        int n = input.n;
        int expected = input.expected;
        int rs = solution(n);
        Assertions.assertEquals(expected, rs);
    }
}
