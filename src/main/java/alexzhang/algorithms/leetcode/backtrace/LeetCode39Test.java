package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给定一个无重复元素的正整数数组 candidates 和一个正整数 target ，找出 candidates 中所有可以使数字和为目标数 target 的
 * 唯一组合。
 * <p>
 * candidates 中的数字可以无限制重复被选取。如果至少一个所选数字数量不同，则两种组合是唯一的。
 * <p>
 * 对于给定的输入，保证和为 target 的唯一组合数少于 150 个。
 * <p>
 * 示例 1：
 * <p>
 * 输入: candidates = [2,3,6,7], target = 7
 * 输出: [[7],[2,2,3]]
 * <p>
 * 示例 2：
 * <p>
 * 输入: candidates = [2,3,5], target = 8
 * <p>
 * 输出: [[2,2,2,2],[2,3,3],[3,5]]
 * <p>
 * 示例 3：
 * <p>
 * 输入: candidates = [2], target = 1
 * <p>
 * 输出: []
 * <p>
 * 示例 4：
 * <p>
 * 输入: candidates = [1], target = 1
 * <p>
 * 输出: [[1]]
 * <p>
 * 示例 5：
 * <p>
 * 输入: candidates = [1], target = 2
 * <p>
 * 输出: [[1,1]]
 * <p>
 * 提示：
 * <p>
 * 1 <= candidates.length <= 30
 * <p>
 * 1 <= candidates[i] <= 200
 * <p>
 * candidate 中的每个元素都是独一无二的。
 * <p>
 * 1 <= target <= 500
 * <p>
 * Related Topics 数组 回溯 👍 1597 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode39Test {
    List<List<Integer>> ans;
    Deque<Integer> combination;

    private static class Input {
        int[] candidates;
        int target;
        List<List<Integer>> expected;

        public Input(int[] candidates, int target, List<List<Integer>> expected) {
            this.candidates = candidates;
            this.target = target;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        List<List<Integer>> expected;
        Input input;

        expected = new ArrayList<>();
        expected.add(Arrays.asList(2, 2, 3));
        expected.add(Collections.singletonList(7));
        input = new Input(new int[]{2, 3, 6, 7}, 7, expected);
        inputs.add(input);

        expected = new ArrayList<>();
        expected.add(Arrays.asList(2, 2, 2, 2));
        expected.add(Arrays.asList(2, 3, 3));
        expected.add(Arrays.asList(3, 5));
        input = new Input(new int[]{2, 3, 5}, 8, expected);
        inputs.add(input);

        expected = new ArrayList<>();
        expected.add(Arrays.asList(1, 1, 1, 1));
        expected.add(Arrays.asList(1, 1, 2));
        expected.add(Arrays.asList(2, 2));
        input = new Input(new int[]{1, 2}, 4, expected);
        inputs.add(input);

        expected = new ArrayList<>();
        expected.add(Arrays.asList(2, 2, 2, 2, 1));
        expected.add(Arrays.asList(2, 2, 2, 3));
        expected.add(Arrays.asList(2, 2, 2, 1, 1, 1));
        expected.add(Arrays.asList(2, 2, 3, 1, 1));
        expected.add(Arrays.asList(2, 2, 5));
        expected.add(Arrays.asList(2, 2, 1, 1, 1, 1, 1));
        expected.add(Arrays.asList(2, 7));
        expected.add(Arrays.asList(2, 6, 1));
        expected.add(Arrays.asList(2, 3, 3, 1));
        expected.add(Arrays.asList(2, 3, 1, 1, 1, 1));
        expected.add(Arrays.asList(2, 5, 1, 1));
        expected.add(Arrays.asList(2, 1, 1, 1, 1, 1, 1, 1));
        expected.add(Arrays.asList(7, 1, 1));
        expected.add(Arrays.asList(6, 3));
        expected.add(Arrays.asList(6, 1, 1, 1));
        expected.add(Arrays.asList(3, 3, 3));
        expected.add(Arrays.asList(3, 3, 1, 1, 1));
        expected.add(Arrays.asList(3, 5, 1));
        expected.add(Arrays.asList(3, 1, 1, 1, 1, 1, 1));
        expected.add(Arrays.asList(5, 1, 1, 1, 1));
        expected.add(Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1));
        input = new Input(new int[]{2, 7, 6, 3, 5, 1}, 9, expected);
        inputs.add(input);

        return inputs;
    }

    private void solve(int[] candidates, int target, int startPoint) {
        for (int i = startPoint; i < candidates.length; i++) {
            combination.addLast(candidates[i]);
            int sum = combination.stream().reduce(0, Integer::sum);
            if (sum == target) {
                ArrayList<Integer> list = new ArrayList<>(combination);
                Collections.sort(list);
                if (!ans.contains(list)) {
                    ans.add(new ArrayList<>(combination));
                }
            }
            if (sum < target) {
                solve(candidates, target, i);
            }
            if (!combination.isEmpty()) {
                combination.removeLast();
            }
        }
    }

    private void solve2(int[] candidates, int target, int startPoint) {
        if (target == 0) {
            ans.add(new ArrayList<>(combination));
            return;
        }
        for (int i = startPoint; i < candidates.length; i++) {
            if ((target - candidates[i]) >= 0) {
                combination.addLast(candidates[i]);
                solve2(candidates, target - candidates[i], i);
                combination.removeLast();
            }
        }
    }

    private void solve3(int[] candidates, int target, int idx) {
        if (idx == candidates.length) {
            return;
        }
        if (target == 0) {
            ans.add(new ArrayList<>(combination));
            return;
        }
        solve3(candidates, target, idx + 1);
        if (target - candidates[idx] >= 0) {
            combination.addLast(candidates[idx]);
            solve3(candidates, target - candidates[idx], idx);
            combination.removeLast();
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] candidates = input.candidates;
        int target = input.target;
        List<List<Integer>> expected = input.expected;
        ans = new ArrayList<>();
        combination = new ArrayDeque<>();
        solve(candidates, target, 0);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test2(Input input) {
        int[] candidates = input.candidates;
        int target = input.target;
        List<List<Integer>> expected = input.expected;
        ans = new ArrayList<>();
        combination = new ArrayDeque<>();
        solve2(candidates, target, 0);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test3(Input input) {
        int[] candidates = input.candidates;
        int target = input.target;
        List<List<Integer>> expected = input.expected;
        ans = new ArrayList<>();
        combination = new ArrayDeque<>();
        solve3(candidates, target, 0);
        System.out.println(ans);
        Collections.reverse(ans);
        Assertions.assertEquals(expected, ans);
    }
}
