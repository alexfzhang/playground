package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * 给你一个整数 n ，请你生成并返回所有由 n 个节点组成且节点值从 1 到 n 互不相同的不同 二叉搜索树 。可以按 任意顺序 返回答案。
 * <p>
 * 示例 1：
 * <img alt="" src="https://assets.leetcode.com/uploads/2021/01/18/uniquebstn3.jpg" style="width: 600px; height: 148px;" />
 * <p>
 * <p>
 * 输入：n = 3
 * <p>
 * 输出：[[1,null,2,null,3],[1,null,3,2],[2,1,3],[3,1,null,null,2],[3,2,null,1]]
 * <p>
 * 示例 2：
 * <p>
 * 输入：n = 1
 * <p>
 * 输出：[[1]]
 * <p>
 * 提示：
 * <p>
 * 1 <= n <= 8
 * <p>
 * Related Topics 树 二叉搜索树 动态规划 回溯 二叉树 👍 1042 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode95Test {
    private boolean[] visited;

    /**
     * Definition for a binary tree node.
     */
    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        @Override
        public String toString() {
            return tree2List(this).toString();
        }
    }

    private static class Input {
        int n;
        List<List<Integer>> expected;

        public Input(int n, List<List<Integer>> expected) {
            this.n = n;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(3, Arrays.asList(Arrays.asList(1, null, 2, null, 3),
                Arrays.asList(1, null, 3, 2),
                Arrays.asList(2, 1, 3),
                Arrays.asList(3, 1, null, null, 2),
                Arrays.asList(3, 2, null, 1))
        );
        inputs.add(input);

        return inputs;
    }

    public static void copy(TreeNode src, TreeNode dest) {
        if (null == src) {
            return;
        }
        dest.val = src.val;
        if (null != src.left) {
            dest.left = new TreeNode();
            copy(src.left, dest.left);
        }
        if (null != src.right) {
            dest.right = new TreeNode();
            copy(src.right, dest.right);
        }
    }

    private static void vertivalTraveral(List<Integer> lst, TreeNode node) {
        if (null == node) {
            return;
        }
        if (null == node.left) {
            if (null != node.right) {
                lst.add(null);
            }
        } else {
            lst.add(node.left.val);
        }
        if (null == node.right) {
            if (null != node.left) {
                if (null != node.left.left || null != node.left.right) {
                    lst.add(null);
                }
            }
        } else {
            lst.add(node.right.val);
        }
        vertivalTraveral(lst, node.left);
        vertivalTraveral(lst, node.right);
    }

    public static List<Integer> tree2List(TreeNode node) {
        // 层序遍历
        List<Integer> lst = new ArrayList<>();
        lst.add(node.val);
        vertivalTraveral(lst, node);
        return lst;
    }

    private TreeNode add(TreeNode root, int val) {
        TreeNode path = root;
        while (null != path) {
            if (val > path.val) {
                if (null == path.right) {
                    path.right = new TreeNode(val);
                    return path;
                } else {
                    path = path.right;
                }
            } else {
                if (null == path.left) {
                    path.left = new TreeNode(val);
                    return path;
                } else {
                    path = path.left;
                }
            }
        }
        return null;
    }

    private void solve(List<TreeNode> ans, TreeNode root, int n, int k) {
        if (k == n) {
            ans.add(copyTree(root));
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (!visited[i - 1]) {
                visited[i - 1] = true;
                TreeNode path = add(root, i);
                solve(ans, root, n, k + 1);
                visited[i - 1] = false;
                if (null != path) {
                    if (i > path.val) {
                        path.right = null;
                    } else {
                        path.left = null;
                    }
                }
            }
        }
    }

    public TreeNode copyTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode newRoot = new TreeNode(root.val);
        newRoot.left = copyTree(root.left);
        newRoot.right = copyTree(root.right);
        return newRoot;
    }

    public List<TreeNode> generateTrees(int n) {
        List<TreeNode> ans = new ArrayList<>();
        visited = new boolean[n];
        for (int i = 1; i <= n; i++) {
            visited[i - 1] = true;
            TreeNode root = new TreeNode(i);
            solve(ans, root, n, 1);
            visited[i - 1] = false;
        }
        return ans;
    }

    /**
     * 二叉搜索树关键的性质是根节点的值大于左子树所有节点的值，小于右子树所有节点的值，且左子树和右子树也同样为二叉搜索树。因此在生成所有可行的二叉搜索树的时候，假设当前序列长度为 n，如果我们枚举根节点的值为 i，那么根据二叉搜索树的性质我们可以知道左子树的节点值的集合为 [1…i−1]，右子树的节点值的集合为 [i+1…n]。而左子树和右子树的生成相较于原问题是一个序列长度缩小的子问题，因此我们可以想到用回溯的方法来解决这道题目。
     * <p>
     * 我们定义 generateTrees(start, end) 函数表示当前值的集合为 [start,end]，返回序列 [start,end] 生成的所有可行的二叉搜索树。按照上文的思路，我们考虑枚举 [start,end] 中的值 i 为当前二叉搜索树的根，那么序列划分为了 [start,i−1] 和 [i+1,end] 两部分。我们递归调用这两部分，即 generateTrees(start, i - 1) 和 generateTrees(i + 1, end)，获得所有可行的左子树和可行的右子树，那么最后一步我们只要从可行左子树集合中选一棵，再从可行右子树集合中选一棵拼接到根节点上，并将生成的二叉搜索树放入答案数组即可。
     * <p>
     * 递归的入口即为 generateTrees(1, n)，出口为当 start > end 的时候，当前二叉搜索树为空，返回空节点即可。
     *
     * @param start start
     * @param end   end
     * @return node list
     */
    public List<TreeNode> generateTrees(int start, int end) {
        List<TreeNode> ans = new ArrayList<>();
        if (start > end) {
            ans.add(null);
            return ans;
        }
        for (int i = start; i <= end; i++) {
            List<TreeNode> leftTrees = generateTrees(start, i - 1);
            List<TreeNode> rightTrees = generateTrees(i + 1, end);

            for (TreeNode leftTree : leftTrees) {
                for (TreeNode rightTree : rightTrees) {
                    TreeNode node = new TreeNode(i);
                    node.left = leftTree;
                    node.right = rightTree;
                    ans.add(node);
                }
            }
        }
        return ans;
    }

    public List<TreeNode> generateTrees2(int n) {
        if (n == 0) {
            return new ArrayList<>();
        }
        return generateTrees(1, n);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int n = input.n;
        List<List<Integer>> expected = input.expected;
        List<TreeNode> treeNodes = generateTrees2(n);
        List<List<Integer>> ans = treeNodes.stream().map(LeetCode95Test::tree2List).collect(Collectors.toList());
        Assertions.assertEquals(expected, ans);
    }
}
