package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给你二叉树的根节点 root 和一个整数目标和 targetSum ，找出所有 从根节点到叶子节点 路径总和等于给定目标和的路径。
 * <p>
 * 叶子节点 是指没有子节点的节点。
 * <p>
 * 示例 1：
 * <p>
 * 输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
 * <p>
 * 输出：[[5,4,11,2],[5,8,4,5]]
 * <p>
 * <p>
 * 示例 2：
 * <p>
 * 输入：root = [1,2,3], targetSum = 5
 * <p>
 * 输出：[]
 * <p>
 * 示例 3：
 * <p>
 * 输入：root = [1,2], targetSum = 0
 * <p>
 * 输出：[]
 * <p>
 * 提示：
 * <p>
 * 树中节点总数在范围 [0, 5000] 内
 * <p>
 * -1000 <= Node.val <= 1000
 * <p>
 * -1000 <= targetSum <= 1000
 * <p>
 * Related Topics 树 深度优先搜索 回溯 二叉树 👍 615 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode113Test {

    /**
     * Definition for a binary tree node.
     */
    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        @Override
        public String toString() {
            return tree2List(this).toString();
        }
    }

    private static void vertivalTraveral(List<Integer> lst, TreeNode node) {
        if (null == node) {
            return;
        }
        if (null == node.left) {
            if (null != node.right) {
                lst.add(null);
            }
        } else {
            lst.add(node.left.val);
        }
        if (null == node.right) {
            if (null != node.left) {
                if (null != node.left.left || null != node.left.right) {
                    lst.add(null);
                }
            }
        } else {
            lst.add(node.right.val);
        }
        vertivalTraveral(lst, node.left);
        vertivalTraveral(lst, node.right);
    }

    private static List<Integer> tree2List(TreeNode node) {
        // 层序遍历
        List<Integer> lst = new ArrayList<>();
        lst.add(node.val);
        vertivalTraveral(lst, node);
        return lst;
    }

    private static TreeNode create(List<Integer> nums, int k, int rootIdx) {
        if (rootIdx >= nums.size() || null == nums.get(rootIdx)) {
            return null;
        }
        TreeNode root = new TreeNode(nums.get(rootIdx));
        root.left = create(nums, k + 1, rootIdx * 2 + 1);
        root.right = create(nums, k + 1, rootIdx * 2 + 2);
        return root;
    }

    private static class Input {
        TreeNode root;
        int targetSum;
        List<List<Integer>> expected;

        public Input(TreeNode root, int targetSum, List<List<Integer>> expected) {
            this.root = root;
            this.targetSum = targetSum;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        TreeNode root = create(Arrays.asList(5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, null, 5, 1), 1, 0);
        input = new Input(root, 22, Arrays.asList(Arrays.asList(5, 4, 11, 2), Arrays.asList(5, 8, 4, 5)));
        inputs.add(input);

        return inputs;
    }

    private void solution(List<List<Integer>> ans, Deque<Integer> row, TreeNode node, int target, int acc) {
        row.add(node.val);
        acc += node.val;
        if (null == node.left && null == node.right) {
            if (acc == target) {
                ans.add(new ArrayList<>(row));
            }
            row.removeLast();
            return;
        } else {
            if (null != node.left) {
                solution(ans, row, node.left, target, acc);
            }
            if (null != node.right) {
                solution(ans, row, node.right, target, acc);
            }
        }
        row.removeLast();
    }

    private List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> ans = new ArrayList<>();
        Deque<Integer> row = new ArrayDeque<>();
        solution(ans, row, root, targetSum, 0);
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        TreeNode root = input.root;
        int targetSum = input.targetSum;
        List<List<Integer>> expected = input.expected;
        List<List<Integer>> ans = pathSum(root, targetSum);
        Assertions.assertEquals(expected, ans);
    }
}
