package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

/**
 * 二进制手表顶部有 4 个 LED 代表 小时（0-11），底部的 6 个 LED 代表 分钟（0-59）。每个 LED 代表一个 0 或 1，最低位在右侧。
 * <p>
 * 例如，下面的二进制手表读取 "3:25" 。
 * <p>
 * （图源：WikiMedia - Binary clock samui moon.jpg ，许可协议：Attribution-ShareAlike 3.0
 * Unported (CC BY-SA 3.0) ）
 * <p>
 * 给你一个整数 turnedOn ，表示当前亮着的 LED 的数量，返回二进制手表可以表示的所有可能时间。你可以 按任意顺序 返回答案。
 * <p>
 * 小时不会以零开头：
 * <p>
 * 例如，"01:00" 是无效的时间，正确的写法应该是 "1:00" 。
 * <p>
 * 分钟必须由两位数组成，可能会以零开头：
 * <p>
 * 例如，"10:2" 是无效的时间，正确的写法应该是 "10:02" 。
 * <p>
 * 示例 1：
 * <p>
 * 输入：turnedOn = 1
 * <p>
 * 输出：["0:01","0:02","0:04","0:08","0:16","0:32","1:00","2:00","4:00","8:00"]
 * <p>
 * 示例 2：
 * <p>
 * 输入：turnedOn = 9
 * 输出：[]
 * <p>
 * 提示：
 * <p>
 * 0 <= turnedOn <= 10
 * <p>
 * Related Topics 位运算 回溯 👍 333 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode401Test {
    private char[] digits;
    private final int TEN = 10;

    private static class Input {
        int turnedOn;
        List<String> expected;

        public Input(int turnedOn, List<String> expected) {
            this.turnedOn = turnedOn;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;
        input = new Input(1, Arrays.asList("0:01", "0:02", "0:04", "0:08", "0:16", "0:32", "1:00", "2:00", "4:00", "8:00"));
        inputs.add(input);
        return inputs;
    }

    private String digits2Clock(char[] digits) {
        String clock = null;
        char[] hour = new char[4];
        System.arraycopy(digits, 0, hour, 0, 4);
        int h = Integer.parseInt(new String(hour), 2);
        if (h >= 0 && h <= 11) {
            char[] min = new char[6];
            System.arraycopy(digits, 4, min, 0, 6);
            int m = Integer.parseInt(new String(min), 2);
            if (m >= 0 && m <= 59) {
                clock = h + ":" + String.format("%02d", m);
            }
        }
        return clock;
    }

    private void read(List<String> ans, int pos, int k, int turnedOn) {
        if ((TEN - pos) < (turnedOn - k)) {
            return;
        }
        if (k == turnedOn) {
            String clock = digits2Clock(digits);
            if (null != clock) {
                ans.add(clock);
            }
            return;
        }
        for (int i = pos; i < TEN; i++) {
            if (digits[i] != '1') {
                digits[i] = '1';
                read(ans, i + 1, k + 1, turnedOn);
                digits[i] = '0';
            }
        }
    }

    private List<String> solution2(int turnedOn) {
        final int maxH = 12;
        final int maxM = 60;
        List<String> ans = new ArrayList<>();
        for (int i = 0; i < maxH; i++) {
            for (int j = 0; j < maxM; j++) {
                if ((Integer.bitCount(i) + Integer.bitCount(j)) == turnedOn) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(i).append(":");
                    if (j < 10) {
                        sb.append("0");
                    }
                    sb.append(j);
                    ans.add(sb.toString());
                }
            }
        }
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void readBinaryWatch(Input input) {
        int turnedOn = input.turnedOn;
        final int maxDigits = 8;
        List<String> ans = new ArrayList<>();
        List<String> expected = input.expected;
        if (turnedOn <= maxDigits && turnedOn >= 0) {
            digits = new char[TEN];
            Arrays.fill(digits, '0');
            read(ans, 0, 0, turnedOn);
        }
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void solution2Test(Input input) {
        int turnedOn = input.turnedOn;
        List<String> expected = input.expected;
        List<String> ans = solution2(turnedOn);
        Assertions.assertEquals(expected, ans);
    }
}
