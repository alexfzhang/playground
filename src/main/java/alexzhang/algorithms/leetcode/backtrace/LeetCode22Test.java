package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。
 * <p>
 * 有效括号组合需满足：左括号必须以正确的顺序闭合。
 * <p>
 * 示例 1：
 * <p>
 * 输入：n = 3
 * <p>
 * 输出：["((()))","(()())","(())()","()(())","()()()"]
 * <p>
 * 示例 2：
 * <p>
 * 输入：n = 1
 * <p>
 * 输出：["()"]
 * <p>
 * 提示：
 * <p>
 * 1 <= n <= 8
 * <p>
 * Related Topics 字符串 动态规划 回溯 👍 2114 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode22Test {
    private List<String> ans;
    private StringBuilder sb;
    private boolean[] visited;
    final int two = 2;

    private static class Input {
        int n;
        List<String> expected;

        public Input(int n, List<String> expected) {
            this.n = n;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(3, Arrays.asList("((()))", "(()())", "(())()", "()(())", "()()()"));
        inputs.add(input);

        input = new Input(1, Collections.singletonList("()"));
        inputs.add(input);

        input = new Input(6, Arrays.asList("()(()())(())", "()(((())))()", "()((()))()()", "((()()(())))", "(()((())))()", "()(())(())()", "(()(()))()()", "(()())(())()", "(((())())())", "((())(()))()", "((()))()()()", "()((())())()", "(((()())()))", "(()(())())()", "((()(())))()", "(()()()()())", "((()))(())()", "(()(()(())))", "(())()(()())", "()(()(()()))", "()((()())())", "()()()()()()", "(()()(()))()", "(((()))())()", "((()()()()))", "(())((()))()", "()(())()()()", "((()())())()", "(())(()(()))", "((((()))()))", "(()(())()())", "(((())(())))", "(()((())()))", "(()()()(()))", "()()(())(())", "(()())((()))", "((((()()))))", "()()()(())()", "(())()(())()", "(((()()())))", "(()(((()))))", "((())()(()))", "()(((()())))", "()()((())())", "(()(()()()))", "()((()()()))", "((()())()())", "()((())(()))", "((())((())))", "()(()())()()", "((())())(())", "()(((()))())", "((()(())()))", "((()()))()()", "()(())(()())", "()((()(())))", "((()))((()))", "()(()()()())", "(()()(())())", "((())(())())", "((()((()))))", "()(()(()))()", "(((()))(()))", "()()(()(()))", "((()()()))()", "(())((()()))", "(()()())(())", "(((((())))))", "(())(()())()", "(())()()()()", "()()(((())))", "((((()))))()", "(((()()))())", "(((())))(())", "((())())()()", "(()()()())()", "(())(((())))", "()()()((()))", "()(()(())())", "(())()((()))", "()(())()(())", "((())()())()", "(()(()()))()", "()((()()))()", "(()())()(())", "(())(()()())", "()((()))(())", "(()((()())))", "(()()())()()", "(())(())()()", "(((())()()))", "((()))()(())", "((()(()())))", "()(()()())()", "()()((()()))", "()()(()())()", "(()()((())))", "((()(()))())", "()(((())()))", "((()()))(())", "((()))(()())", "()(())((()))", "((())()()())", "(())(())(())", "()((((()))))", "((())(()()))", "((()()())())", "()()()()(())", "((((())())))", "(()())(()())", "(())((())())", "()()(()()())", "(()((()))())", "(())()()(())", "(((()())))()", "(()(())(()))", "(((())))()()", "()()(())()()", "((((())))())", "(((()))()())", "(((()(()))))", "(()(()))(())", "()()()(()())", "(((())()))()", "(()()(()()))", "()(()()(()))", "()()((()))()", "(()(()())())", "()((())()())", "((()())(()))", "(()())()()()", "()(()((())))"));
        inputs.add(input);

        return inputs;
    }

    private boolean validation(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(c);
            } else {
                if (stack.isEmpty() || stack.pop() != '(') {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    private void solution1(String parentnesses, int idx) {
        if (idx == parentnesses.length()) {
            if (validation(sb.toString())) {
                ans.add(sb.toString());
            }
            return;
        }
        for (int i = 0; i < parentnesses.length(); i++) {
            if (!visited[i]) {
                sb.append(parentnesses.charAt(i));
                visited[i] = true;
                solution1(parentnesses, idx + 1);
                visited[i] = false;
                sb.deleteCharAt(sb.length() - 1);
            }
        }
    }

    private void solution2(int n, int open, int close) {
        if (sb.length() == n * two) {
            ans.add(sb.toString());
            return;
        }
        if (open < n) {
            sb.append("(");
            solution2(n, open + 1, close);
            sb.deleteCharAt(sb.length() - 1);
        }
        if(close < open) {
            sb.append(")");
            solution2(n, open, close + 1);
            sb.deleteCharAt(sb.length() - 1);
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int n = input.n;
        List<String> expected = input.expected;
        ans = new ArrayList<>();
        sb = new StringBuilder();
        visited = new boolean[n * 2];
//        solution1(new String(chars), 0);
        solution2(n, 0, 0);
        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }
}
