package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给定一个可包含重复数字的序列 nums ，按任意顺序 返回所有不重复的全排列。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [1,1,2]
 * <p>
 * 输出：
 * <p>
 * [[1,1,2],
 * <p>
 * [1,2,1],
 * <p>
 * [2,1,1]]
 *
 * <p>
 * 示例 2：
 * <p>
 * 输入：nums = [1,2,3]
 * <p>
 * 输出：
 * <p>
 * [[1,2,3],
 * <p>
 * [1,3,2],
 * <p>
 * [2,1,3],
 * <p>
 * [2,3,1],
 * <p>
 * [3,1,2],
 * <p>
 * [3,2,1]]
 *
 * <p>
 * 提示：
 * <p>
 * 1 <= nums.length <= 8
 * <p>
 * -10 <= nums[i] <= 10
 * <p>
 * Related Topics 数组 回溯 👍 832 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode47Test {
    private boolean[] visited;

    private static class Input {
        int[] nums;
        List<List<Integer>> expected;

        public Input(int[] nums, List<List<Integer>> expected) {
            this.nums = nums;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;
        input = new Input(new int[]{2, 1, 1, 1},
                Arrays.asList(
                        Arrays.asList(1, 1, 1, 2),
                        Arrays.asList(1, 1, 2, 1),
                        Arrays.asList(1, 2, 1, 1),
                        Arrays.asList(2, 1, 1, 1)
                )
        );
        inputs.add(input);
        input = new Input(new int[]{1, 1, 2},
                Arrays.asList(
                        Arrays.asList(1, 1, 2),
                        Arrays.asList(1, 2, 1),
                        Arrays.asList(2, 1, 1)
                )
        );
        inputs.add(input);
        input = new Input(new int[]{1, 2, 3},
                Arrays.asList(
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 3, 2),
                        Arrays.asList(2, 1, 3),
                        Arrays.asList(2, 3, 1),
                        Arrays.asList(3, 1, 2),
                        Arrays.asList(3, 2, 1)
                )
        );
        inputs.add(input);
        return inputs;
    }

    private void swap(int[] nums, int a, int b) {
        if (a != b) {
            int t = nums[a];
            nums[a] = nums[b];
            nums[b] = t;
        }
    }

    private void solveUnique(List<List<Integer>> ans, List<Integer> permutation, int[] nums, int idx) {
        if (idx == nums.length) {
            ans.add(new ArrayList<>(permutation));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            // 保证：从左往右第一个未被填过的数字
            // skip duplications if (i > 0 && nums[i] == nums[i - 1] && !vis[i - 1]) continue
            // skip if (visited[i]) continue
            if (!visited[i]) {
                if (i == 0 || nums[i] != nums[i - 1] || visited[i - 1]) {
                    permutation.add(nums[i]);
                    visited[i] = true;
                    solve(ans, permutation, nums, idx + 1);
                    visited[i] = false;
                    permutation.remove(idx);
                }
            }
        }
    }

    private void solve(List<List<Integer>> ans, List<Integer> permutation, int[] nums, int idx) {
        if (idx == nums.length) {
            ans.add(new ArrayList<>(permutation));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (visited[i]) {
                continue;
            }
            if (i > 0 && nums[i] == nums[i - 1] && !visited[i - 1]) {
                continue;
            }
            permutation.add(nums[i]);
            visited[i] = true;
            solve(ans, permutation, nums, idx + 1);
            visited[i] = false;
            permutation.remove(idx);
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] nums = input.nums;
        List<List<Integer>> expected = input.expected;
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> permutation = new ArrayList<>();
        visited = new boolean[nums.length];
        Arrays.sort(nums);
        solve(ans, permutation, nums, 0);
        System.out.println("ans: " + ans);
        Assertions.assertEquals(expected, ans);
    }
}
