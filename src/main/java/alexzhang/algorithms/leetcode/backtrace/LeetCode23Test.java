//给你一个链表数组，每个链表都已经按升序排列。
//
// 请你将所有链表合并到一个升序链表中，返回合并后的链表。
//
//
//
// 示例 1：
//
// 输入：lists = [[1,4,5],[1,3,4],[2,6]]
//输出：[1,1,2,3,4,4,5,6]
//解释：链表数组如下：
//[
//  1->4->5,
//  1->3->4,
//  2->6
//]
//将它们合并到一个有序链表中得到。
//1->1->2->3->4->4->5->6
//
//
// 示例 2：
//
// 输入：lists = []
//输出：[]
//
//
// 示例 3：
//
// 输入：lists = [[]]
//输出：[]
//
//
//
//
// 提示：
//
//
// k == lists.length
// 0 <= k <= 10^4
// 0 <= lists[i].length <= 500
// -10^4 <= lists[i][j] <= 10^4
// lists[i] 按 升序 排列
// lists[i].length 的总和不超过 10^4
//
// Related Topics 链表 分治 堆（优先队列） 归并排序
// 👍 1394 👎 0

package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode23Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode[] lists;
        private final ListNode expected;

        public Input(ListNode[] lists, ListNode expected) {
            this.lists = lists;
            this.expected = expected;
        }

        public ListNode[] getLists() {
            return lists;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(new ListNode[]{}, ListNode.create()),
                new Input(new ListNode[]{ListNode.create(1, 4, 5), ListNode.create(1, 4, 5)}, ListNode.create(1, 1, 4, 4, 5, 5)),
                new Input(new ListNode[]{ListNode.create(1, 4, 5), ListNode.create(1, 3, 4), ListNode.create(2, 6)}, ListNode.create(1, 1, 2, 3, 4, 4, 5, 6))
        };
    }

    private ListNode merge(ListNode a, ListNode b) {
        ListNode m = null;
        if (null != a && null != b) {
            ListNode p = null;
            while (null != a && null != b) {
                if (a.val > b.val) {
                    if (null == m) {
                        m = b;
                        p = m;
                    } else {
                        p.next = b;
                        p = p.next;
                    }
                    b = b.next;
                } else {
                    if (null == m) {
                        m = a;
                        p = m;
                    } else {
                        p.next = a;
                        p = p.next;
                    }
                    a = a.next;
                }
                p.next = null;
            }
            while (null != a) {
                p.next = a;
                p = p.next;
                a = a.next;
            }
            while (null != b) {
                p.next = b;
                p = p.next;
                b = b.next;
            }
        } else if (null != a) {
            m = a;
        } else if (null != b) {
            m = b;
        }
        return m;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void mergekLists(Input input) {
        ListNode[] lists = input.getLists();
        ListNode expected = input.getExpected();
        ListNode head;
        if (lists.length == 0) {
            head = null;
        } else if (lists.length == 1) {
            head = lists[0];
        } else {
            head = null;
            for (ListNode list : lists) {
                head = merge(head, list);
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
