package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 给定一个只包含数字的字符串 s ，用以表示一个 IP 地址，返回所有可能从 s 获得的 有效 IP 地址 。你可以按 任何 顺序返回答案。
 * <p>
 * 有效 IP 地址 正好由四个整数（每个整数位于 0 到 255 之间组成，且不能含有前导 0），整数之间用 '.' 分隔。
 * <p>
 * 例如："0.1.2.201" 和 "192.168.1.1" 是 有效 IP 地址，但是 "0.011.255.245"、"192.168.1.312"
 * <p>
 * 和 "192.168@1.1" 是 无效 IP 地址。
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "25525511135"
 * <p>
 * 输出：["255.255.11.135","255.255.111.35"]
 * <p>
 * 示例 2：
 * <p>
 * 输入：s = "0000"
 * <p>
 * 输出：["0.0.0.0"]
 * <p>
 * 示例 3：
 * <p>
 * 输入：s = "1111"
 * <p>
 * 输出：["1.1.1.1"]
 * <p>
 * 示例 4：
 * <p>
 * 输入：s = "010010"
 * <p>
 * 输出：["0.10.0.10","0.100.1.0"]
 * <p>
 * 示例 5：
 * <p>
 * 输入：s = "101023"
 * <p>
 * 输出：["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
 * <p>
 * 提示：
 * <p>
 * 0 <= s.length <= 3000
 * <p>
 * s 仅由数字组成
 * <p>
 * Related Topics 字符串 回溯 👍 721 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode93Test {
    private static final int FOUR = 4;
    private static final int TEN = 10;
    private static final int ELEVEN = 11;
    private static final int TWELVE = 12;

    private static class Input {
        String s;
        List<String> expected;

        public Input(String s, List<String> expected) {
            this.s = s;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input("19216811", Arrays.asList("1.92.168.11", "19.2.168.11", "19.21.68.11", "19.216.8.11", "19.216.81.1", "192.1.68.11", "192.16.8.11", "192.16.81.1", "192.168.1.1"));
        inputs.add(input);

        input = new Input("25525511135", Arrays.asList("255.255.11.135", "255.255.111.35"));
        inputs.add(input);

        input = new Input("0000", Collections.singletonList("0.0.0.0"));
        inputs.add(input);

        input = new Input("1111", Collections.singletonList("1.1.1.1"));
        inputs.add(input);

        input = new Input("010010", Arrays.asList("0.10.0.10", "0.100.1.0"));
        inputs.add(input);

        input = new Input("101023", Arrays.asList("1.0.10.23", "1.0.102.3", "10.1.0.23", "10.10.2.3", "101.0.2.3"));
        inputs.add(input);

        return inputs;
    }

    private void solve(List<String> ans, Deque<String> path, String s, int step, int k) {
        if (k > FOUR) {
            if (s.length() == 0) {
                ans.add(String.join(".", path));
            }
            return;
        }
        for (int i = step; i < FOUR && i <= s.length(); i++) {
            String ipPortionStr = s.substring(0, i);
            if (ipPortionStr.length() == 1 || !ipPortionStr.startsWith("0")) {
                int ipPortion = Integer.parseInt(ipPortionStr);
                if (ipPortion >= 0 && ipPortion <= 255) {
                    path.add(ipPortionStr);
                    solve(ans, path, s.substring(i), step, k + 1);
                    path.removeLast();
                }
            }
        }
    }

    private List<String> restoreIpAddresses(String s) {
        List<String> ans = new ArrayList<>();
        Deque<String> path = new ArrayDeque<>();
        int len = s.length();
        int step = 0;
        if (len < FOUR || len > TWELVE) {
            return ans;
        }
        if (len <= TEN) {
            step = 1;
        }
        if (len == ELEVEN) {
            step = 2;
        }
        if (len == TWELVE) {
            step = 3;
        }
        solve(ans, path, s, step, 1);
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        String s = input.s;
        List<String> expected = input.expected;
        List<String> rs = restoreIpAddresses(s);
        System.out.println(s + ": " + rs);
        Assertions.assertEquals(expected, rs);
    }
}
