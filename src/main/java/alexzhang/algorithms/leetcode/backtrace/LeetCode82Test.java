//存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除链表中所有存在数字重复情况的节点，只保留原始链表中 没有重复出现 的数字。
//
// 返回同样按升序排列的结果链表。
//
//
//
// 示例 1：
//
//
//输入：head = [1,2,3,3,4,4,5]
//输出：[1,2,5]
//
//
// 示例 2：
//
//
//输入：head = [1,1,1,2,3]
//输出：[2,3]
//
//
//
//
// 提示：
//
//
// 链表中节点数目在范围 [0, 300] 内
// -100 <= Node.val <= 100
// 题目数据保证链表已经按升序排列
//
// Related Topics 链表 双指针
// 👍 662 👎 0

package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode82Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l;
        private final ListNode expected;

        public Input(ListNode l, ListNode expected) {
            this.l = l;
            this.expected = expected;
        }

        public ListNode getL() {
            return l;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create()),
                new Input(ListNode.create(1), ListNode.create(1)),
                new Input(ListNode.create(2, 2), ListNode.create()),
                new Input(ListNode.create(1, 2), ListNode.create(1, 2)),
                new Input(ListNode.create(1, 2, 2), ListNode.create(1)),
                new Input(ListNode.create(1, 2, 3, 3, 4, 4, 5), ListNode.create(1, 2, 5)),
                new Input(ListNode.create(1, 1, 1, 2, 3), ListNode.create(2, 3))
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void deleteDuplicates(Input input) {
        ListNode head = input.getL();
        final ListNode expected = input.getExpected();
        if (null != head && null != head.next) {
            ListNode a = head;
            ListNode b = head.next;
            ListNode t = head;
            boolean isDuplicate = false;
            while (null != b) {
                while (null != b && a.val == b.val) {
                    b = b.next;
                    a.next = b;
                    isDuplicate = true;
                }
                if (isDuplicate) {
                    // delete first
                    if(a == head) {
                        head = b;
                    } else {
                        //
                        t.next = b;
                        a = b;
                        if(null != b) {
                            b = b.next;
                        }
                    }
                    isDuplicate = false;
                } else {
                    t = a;
                    a = a.next;
                    b = b.next;
                }
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
