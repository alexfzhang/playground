package alexzhang.algorithms.leetcode.backtrace;

import alexzhang.algorithms.datastructure.ListNode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * 对链表进行插入排序。
 * <p>
 * 插入排序的动画演示如上。从第一个元素开始，该链表可以被认为已经部分排序（用黑色表示）。
 * <p>
 * 每次迭代时，从输入数据中移除一个元素（用红色表示），并原地将其插入到已排好序的链表中。
 * <p>
 * 插入排序算法：
 * <p>
 * 插入排序是迭代的，每次只移动一个元素，直到所有元素可以形成一个有序的输出列表。
 * <p>
 * 每次迭代中，插入排序只从输入数据中移除一个待排序的元素，找到它在序列中适当的位置，并将其插入。
 * <p>
 * 重复直到所有输入数据插入完为止。
 * <p>
 * 示例 1：
 * <p>
 * 输入: 4->2->1->3
 * <p>
 * 输出: 1->2->3->4
 * <p>
 * 示例 2：
 * <p>
 * 输入: -1->5->3->4->0
 * <p>
 * 输出: -1->0->3->4->5
 * <p>
 * Related Topics 链表 排序 👍 443 👎 0
 *
 * @author alexzhang
 */
public class LeetCode147Test {
    private static class Input {
        private final ListNode head;
        private final ListNode expected;

        public Input(ListNode head, ListNode expected) {
            this.head = head;
            this.expected = expected;
        }

        public ListNode getHead() {
            return head;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create()),
                new Input(ListNode.create(1), ListNode.create(1)),
                new Input(ListNode.create(1, 2), ListNode.create(1, 2)),
                new Input(ListNode.create(1, 2, 3), ListNode.create(1, 2, 3)),
                new Input(ListNode.create(3, 2, 1), ListNode.create(1, 2, 3)),
                new Input(ListNode.create(5, 4, 3, 2, 1), ListNode.create(1, 2, 3, 4, 5)),
                new Input(ListNode.create(3, 2, 1, 6, 5, 4, 7), ListNode.create(1, 2, 3, 4, 5, 6, 7))
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
        ListNode head = input.getHead();
        ListNode expected = input.getExpected();
        if (null != head && null != head.next) {
            ListNode h = null;
            ListNode p = head;
            while (null != p) {
                ListNode c = p;
                p = p.next;
                if (null == h || c.val <= h.val) {
                    c.next = h;
                    h = c;
                } else {
                    ListNode pp = h;
                    while (true) {
                        if (null == pp.next || c.val < pp.next.val) {
                            c.next = pp.next;
                            pp.next = c;
                            break;
                        }
                        pp = pp.next;
                    }
                }
            }
            Assertions.assertEquals(expected, h);
        } else {
            Assertions.assertEquals(expected, head);
        }
    }
}
