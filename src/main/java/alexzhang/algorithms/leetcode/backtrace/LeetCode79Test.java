package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。
 * <p>
 * 单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。
 * <p>
 * 示例 1：
 * <p>
 * 输入：board = [
 * <p>
 * ["A","B","C","E"],
 * <p>
 * ["S","F","C","S"],
 * <p>
 * ["A","D","E","E"]],
 * <p>
 * word = "ABCCED"
 * <p>
 * 输出：true
 * <p>
 * 示例 2：
 * <p>
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "SEE"
 * <p>
 * 输出：true
 * <p>
 * 示例 3：
 * <p>
 * 输入：board = [["A","B","C","E"],["S","F","C","S"],["A","D","E","E"]], word = "ABCB"
 * <p>
 * 输出：false
 * <p>
 * 提示：
 * <p>
 * m == board.length
 * <p>
 * n = board[i].length
 * <p>
 * 1 <= m, n <= 6
 * <p>
 * 1 <= word.length <= 15
 * <p>
 * board 和 word 仅由大小写英文字母组成
 * <p>
 * 进阶：你可以使用搜索剪枝的技术来优化解决方案，使其在 board 更大的情况下可以更快解决问题？
 * <p>
 * Related Topics 数组 回溯 矩阵 👍 1073 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode79Test {
    private boolean exists;
    private List<Character> w;
    private boolean[][] visited;
    private int count = 0;
    private static final int[][] DIRECTIONS = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    private static class Input {
        char[][] board;
        String word;
        boolean expected;

        public Input(char[][] board, String word, boolean expected) {
            this.board = board;
            this.word = word;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(new char[][]{{'a', 'b'}, {'c', 'd'}}, "abcd", false);
        inputs.add(input);

        input = new Input(new char[][]{{'a'}}, "a", true);
        inputs.add(input);

        input = new Input(new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "ABCCED", true);
        inputs.add(input);

        input = new Input(new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "SEE", true);
        inputs.add(input);

        input = new Input(new char[][]{{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}}, "ABCB", false);
        inputs.add(input);

        return inputs;
    }

    /**
     * @param board board
     * @param word  word
     * @param m     row count
     * @param n     column count
     * @param row   row index
     * @param col   column index
     */
    private void solution1(char[][] board, String word, int m, int n, int row, int col) {
        count += 1;
        if (w.size() == word.length()) {
            exists = true;
            return;
        }
        if (row == m || col == n || row < 0 || col < 0) {
            return;
        }
        if (!visited[row][col]) {
            if (board[row][col] == word.charAt(w.size())) {
                w.add(board[row][col]);
                visited[row][col] = true;
                // left
                if (!exists) {
                    solution1(board, word, m, n, row, col - 1);
                }
                // right
                if (!exists) {
                    solution1(board, word, m, n, row, col + 1);
                }
                // up
                if (!exists) {
                    solution1(board, word, m, n, row - 1, col);
                }
                // down
                if (!exists) {
                    solution1(board, word, m, n, row + 1, col);
                }
                visited[row][col] = false;
                if (w.size() > 0) {
                    w.remove(w.size() - 1);
                }
            }
        }
    }

    private boolean solution2(char[][] board, String word, int m, int n, int row, int col, int k) {
        if (board[row][col] != word.charAt(k)) {
            return false;
        } else if (k == word.length() - 1) {
            return true;
        }
        visited[row][col] = true;
        boolean flag = false;
        for (int[] direction : DIRECTIONS) {
            int newRow = row + direction[0];
            int newCol = col + direction[1];
            if (newRow >= 0 && newRow < m && newCol >= 0 && newCol < n) {
                if (!visited[newRow][newCol]) {
                    boolean b = solution2(board, word, m, n, newRow, newCol, k + 1);
                    if (b) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        visited[row][col] = false;
        return flag;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        char[][] board = input.board;
        String word = input.word;
        boolean expected = input.expected;
        exists = false;
        w = new ArrayList<>(word.length());
        int m = board.length;
        int n = board[0].length;
        visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                solution1(board, word, m, n, i, j);
                System.out.println(i + ", " + j + ": " + count);
                count = 0;
                w.clear();
            }
        }
        Assertions.assertEquals(expected, exists);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test2(Input input) {
        char[][] board = input.board;
        String word = input.word;
        boolean expected = input.expected;
        exists = false;
        int m = board.length;
        int n = board[0].length;
        visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                exists = solution2(board, word, m, n, i, j, 0);
                if (exists) {
                    break;
                }
            }
            if (exists) {
                break;
            }
        }
        Assertions.assertEquals(expected, exists);
    }
}
