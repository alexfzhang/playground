package alexzhang.algorithms.leetcode.backtrace;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * 每一种解法包含一个不同的 n 皇后问题 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。
 * <p>
 * 示例 1：
 * <p>
 * 输入：n = 4
 * <p>
 * 输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
 * <p>
 * 解释：如上图所示，4 皇后问题存在两个不同的解法。
 *
 * <p>
 * 示例 2：
 * <p>
 * 输入：n = 1
 * <p>
 * 输出：[["Q"]]
 * <p>
 * 提示：
 * <p>
 * 1 <= n <= 9
 * <p>
 * 皇后彼此不能相互攻击，也就是说：任何两个皇后都不能处于同一条横行、纵行或斜线上。
 * <p>
 * Related Topics 数组 回溯 👍 1016 👎 0
 *
 * @author Alex F Zhang
 */


public class LeetCode51Test {

    private List<List<String>> solution(int n) {
        char[][] board = new char[n][n];
        for (char[] c : board) {
            Arrays.fill(c, '.');
        }
        List<List<String>> rs = new ArrayList<>();
        solve(rs, board, 0);
        return rs;
    }

    private void solve(List<List<String>> rs, char[][] board, int row) {
        if (row == board.length) {
            rs.add(char2Array2List(board));
            return;
        }
        for (int col = 0; col < board.length; col++) {
            if (validate(board, row, col)) {
                char[][] temp = copy(board);
                temp[row][col] = 'Q';
                solve(rs, temp, row + 1);
            }
        }
    }

    private boolean validate(char[][] board, int row, int column) {
        for (int i = row - 1; i >= 0; i--) {
            if (board[i][column] == 'Q') {
                return false;
            }
        }
        for (int i = row - 1, j = column + 1; i >= 0 && j < board.length; i--, j++) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }
        for (int i = row - 1, j = column - 1; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 'Q') {
                return false;
            }
        }
        return true;
    }

    private char[][] copy(char[][] src) {
        char[][] dest = new char[src.length][src.length];
        for (int i = 0; i < src.length; i++) {
            System.arraycopy(src[i], 0, dest[i], 0, src.length);
        }
        return dest;
    }

    private List<String> char2Array2List(char[][] board) {
        List<String> rt = new ArrayList<>();
        for (char[] charArray : board) {
            StringBuilder builder = new StringBuilder();
            for (char c : charArray) {
                builder.append(c);
            }
            rt.add(builder.toString());
        }
        return rt;
    }

    private static class Input {
        int n;
        List<List<String>> expected;

        public Input(int n, List<List<String>> expected) {
            this.n = n;
            this.expected = expected;
        }

    }

    private static Input[] createInput() {
        try (InputStream resourceAsStream = LeetCode51Test.class.getClassLoader().getResourceAsStream("leetcode_cases/leetcode51_cases.json")) {
            if (null != resourceAsStream) {
                String confStr = IOUtils.toString(resourceAsStream);
                Map<String, Object> map = new JSONObject(confStr).toMap();
                @SuppressWarnings("unchecked")
                Input[] inputs = {
                        new Input(1, new ArrayList<>((List<List<String>>) map.get("1"))),
                        new Input(2, new ArrayList<>((List<List<String>>) map.get("2"))),
                        new Input(3, new ArrayList<>((List<List<String>>) map.get("3"))),
                        new Input(4, new ArrayList<>((List<List<String>>) map.get("4"))),
                        new Input(5, new ArrayList<>((List<List<String>>) map.get("5"))),
                        new Input(6, new ArrayList<>((List<List<String>>) map.get("6"))),
                        new Input(7, new ArrayList<>((List<List<String>>) map.get("7"))),
                        new Input(8, new ArrayList<>((List<List<String>>) map.get("8"))),
                        new Input(9, new ArrayList<>((List<List<String>>) map.get("9")))
                };
                return inputs;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return new Input[]{
                new Input(1, new ArrayList<>()),
                new Input(2, new ArrayList<>()),
                new Input(3, new ArrayList<>()),
                new Input(4, new ArrayList<>()),
                new Input(5, new ArrayList<>()),
                new Input(6, new ArrayList<>()),
                new Input(7, new ArrayList<>()),
                new Input(8, new ArrayList<>()),
                new Input(9, new ArrayList<>())
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void eightQueensTest(Input input) {
        int n = input.n;
        List<List<String>> solution = solution(n);
        Assertions.assertEquals(input.expected, solution);
    }
}
