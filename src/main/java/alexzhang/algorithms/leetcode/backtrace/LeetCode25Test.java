package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode25Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l;
        private final ListNode expected;
        private final int k;

        public Input(ListNode l, ListNode expected, int k) {
            this.l = l;
            this.expected = expected;
            this.k = k;
        }

        public ListNode getL() {
            return l;
        }

        public int getK() {
            return k;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create(), 1),
                new Input(ListNode.create(1), ListNode.create(1), 1),
                new Input(ListNode.create(1, 2), ListNode.create(2, 1), 2),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(2, 1, 4, 3, 5), 2),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(3, 2, 1, 4, 5), 3),
                new Input(ListNode.create(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), ListNode.create(3, 2, 1, 6, 5, 4, 9, 8, 7, 10), 3),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(1, 2, 3, 4, 5), 1)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void reversekGroup(Input input) {
        ListNode head = input.getL();
        ListNode expected = input.getExpected();
        int k = input.getK();
        if (null != head && null != head.next && k > 1) {
            ListNode p = head;
            int length = 0;
            while (null != p) {
                length += 1;
                p = p.next;
            }
            // no. of groups
            int gn = length / k;
            ListNode a = head;
            ListNode h = head;
            ListNode pr = head;
            ListNode b = head.next;
            // iteration count
            int cnt = 0;
            while (gn > 0) {
                if (cnt == (k - 1)) {
                    // after reverse first group, b is first group's head
                    head = h;
                }
                if (cnt > 0 && cnt % (k - 1) == 0) {
                    // avoid loop
                    if (cnt > (k - 1)) {
                        pr.next = h;
                        pr = a;
                    }
                    a = a.next;
                    if(null != a) {
                        b = a.next;
                    }
                    h = a;
                    gn -= 1;
                }
                if (null != b & gn > 0) {
                    a.next = b.next;
                    b.next = h;
                    h = b;
                    b = a.next;
                    cnt += 1;
                }
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
