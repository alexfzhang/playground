//给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。 
//
// 
//
// 示例 1： 
//
// 
//输入：head = [1,2,3,4,5], k = 2
//输出：[4,5,1,2,3]
// 
//
// 示例 2： 
//
// 
//输入：head = [0,1,2], k = 4
//输出：[2,0,1]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目在范围 [0, 500] 内 
// -100 <= Node.val <= 100 
// 0 <= k <= 2 * 109 
// 
// Related Topics 链表 双指针 
// 👍 589 👎 0

package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode61Test {
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l;
        private final ListNode expected;
        private final int k;

        public Input(ListNode l, ListNode expected, int k) {
            this.l = l;
            this.expected = expected;
            this.k = k;
        }

        public ListNode getL() {
            return l;
        }

        public int getK() {
            return k;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create(), 2),
                new Input(ListNode.create(1, 2), ListNode.create(1, 2), 2),
                new Input(ListNode.create(1, 2, 3), ListNode.create(1, 2, 3), 3),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(4, 5, 1, 2, 3), 2),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(3, 4, 5, 1, 2), 3),
                new Input(ListNode.create(0, 1, 2), ListNode.create(1, 2, 0), 5)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
        ListNode head = input.getL();
        ListNode expected = input.getExpected();
        if (null == head) {
            Assertions.assertNull(expected);
        } else if (null == head.next) {
            Assertions.assertEquals(expected, head);
        } else {
            int inputK = input.getK();
            ListNode t = head;
            int length = 0;
            int k;
            while (null != t) {
                length += 1;
                t = t.next;
            }
            if (inputK > length) {
                k = inputK % length;
            } else {
                k = inputK;
            }
            t = head;
            for (int i = 0; i < k && null != t; i++) {
                t = t.next;
            }
            if (null != t) {
                ListNode h = head;
                while (null != t.next) {
                    t = t.next;
                    h = h.next;
                }
                t.next = head;
                head = h.next;
                h.next = null;
            }
            Assertions.assertEquals(expected, head);
        }
    }
}
