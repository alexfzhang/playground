package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * 给你一个字符串 s，找到 s 中最长的回文子串。
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "babad"
 * <p>
 * 输出："bab"
 * <p>
 * 解释："aba" 同样是符合题意的答案。
 * <p>
 * 示例 2：
 * <p>
 * 输入：s = "cbbd"
 * <p>
 * 输出："bb"
 * <p>
 * 示例 3：
 * <p>
 * 输入：s = "a"
 * <p>
 * 输出："a"
 * <p>
 * 示例 4：
 * <p>
 * 输入：s = "ac"
 * <p>
 * 输出："a"
 * <p>
 * 提示：
 * <p>
 * 1 <= s.length <= 1000
 * <p>
 * s 仅由数字和英文字母（大写和/或小写）组成
 * <p>
 * Related Topics 字符串 动态规划 👍 4251 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode5Test {

    private static class Input {
        String s;
        String expected;

        public Input(String s, String expected) {
            this.s = s;
            this.expected = expected;
        }
    }

    public static Input[] createInput() {
        return new Input[]{
                new Input("a", "a"),
                new Input("babad", "bab"),
                new Input("cbbd", "bb"),
                new Input("ac", "a")
        };
    }

    private String solution(String s) {
        final int one = 1;
        final int two = 2;
        String sub;
        if (s.length() == one) {
            sub = s;
        } else if (s.length() == two) {
            sub = s.substring(0, 1);
        } else {
            sub = s.substring(1, 3);
        }
        return sub;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    void findMedianSortedArrays(Input input) {
        String rs = solution(input.s);
        Assertions.assertEquals(input.expected, rs);
    }
}
