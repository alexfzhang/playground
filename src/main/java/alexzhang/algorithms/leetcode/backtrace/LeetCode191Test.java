package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

/**
 * 编写一个函数，输入是一个无符号整数（以二进制串的形式），返回其二进制表达式中数字位数为 '1' 的个数（也被称为汉明重量）。
 * <p>
 * 提示：
 * <p>
 * 请注意，在某些语言（如 Java）中，没有无符号整数类型。在这种情况下，输入和输出都将被指定为有符号整数类型，并且不应影响您的实现，因为无论整数是有符号的
 * <p>
 * 还是无符号的，其内部的二进制表示形式都是相同的。
 * <p>
 * 在 Java 中，编译器使用二进制补码记法来表示有符号整数。因此，在上面的 示例 3 中，输入表示有符号整数 -3。
 * <p>
 * 示例 1：
 * <p>
 * 输入：00000000000000000000000000001011
 * <p>
 * 输出：3
 * <p>
 * 解释：输入的二进制串 00000000000000000000000000001011 中，共有三位为 '1'。
 * <p>
 * 示例 2：
 * <p>
 * 输入：00000000000000000000000010000000
 * <p>
 * 输出：1
 * <p>
 * 解释：输入的二进制串 00000000000000000000000010000000 中，共有一位为 '1'。
 * <p>
 * 示例 3：
 * <p>
 * 输入：11111111111111111111111111111101
 * <p>
 * 输出：31
 * 解释：输入的二进制串 11111111111111111111111111111101 中，共有 31 位为 '1'。
 * <p>
 * 提示：
 * <p>
 * 输入必须是长度为 32 的 二进制串 。
 * <p>
 * 进阶：
 * <p>
 * 如果多次调用这个函数，你将如何优化你的算法？
 * <p>
 * Related Topics 位运算 👍 399 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode191Test {
    private static class Input {
        int n;
        int expected;

        public Input(int n, int expected) {
            this.n = n;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> input = new ArrayList<>();
        input.add(new Input(-3, 31));
        input.add(new Input(11, 3));
        input.add(new Input(128, 1));
        return input;
    }

    public int bitCount(int i) {
        // HD, Figure 5-2
//        i = i - ((i >>> 1) & 0x55555555);
        i = ((i >>> 1) & 0x55555555) + (i & 0x55555555);
        i = (i & 0x33333333) + ((i >>> 2) & 0x33333333);
        i = (i + (i >>> 4)) & 0x0f0f0f0f;
        i = i + (i >>> 8);
        i = i + (i >>> 16);
        return i & 0x3F;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void hammingWeight(Input input) {
        int count = bitCount(input.n);
        int expected = input.expected;
        Assertions.assertEquals(expected, count);
    }
}
