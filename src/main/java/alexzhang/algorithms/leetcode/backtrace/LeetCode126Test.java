package alexzhang.algorithms.leetcode.backtrace;

import alexzhang.algorithms.leetcode.util.CaseReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;

/**
 * 按字典 wordList 完成从单词 beginWord 到单词 endWord 转化，一个表示此过程的 转换序列 是形式上像 beginWord -> s1 -> s2 -> ... -> sk 这样的单词序列，并满足：
 * <p>
 * 每对相邻的单词之间仅有单个字母不同。
 * <p>
 * 转换过程中的每个单词 si（1 <= i <= k）必须是字典 wordList 中的单词。注意，beginWord 不必是字典 wordList 中的单词。
 * <p>
 * sk == endWord
 * <p>
 * 给你两个单词 beginWord 和 endWord ，以及一个字典 wordList 。请你找出并返回所有从 beginWord 到 endWord 的 最短转换序列 ，如果不存在这样的转换序列，返回一个空列表。每个序列都应该以单词列表 [beginWord, s1, s2, ..., sk] 的形式返回。
 * <p>
 * 示例 1：
 * <p>
 * 输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log","cog"]
 * <p>
 * 输出：[["hit","hot","dot","dog","cog"],["hit","hot","lot","log","cog"]]
 * <p>
 * 解释：存在 2 种最短的转换序列：
 * <p>
 * "hit" -> "hot" -> "dot" -> "dog" -> "cog"
 * <p>
 * "hit" -> "hot" -> "lot" -> "log" -> "cog"
 * <p>
 * 示例 2：
 * <p>
 * 输入：beginWord = "hit", endWord = "cog", wordList = ["hot","dot","dog","lot","log"]
 * <p>
 * 输出：[]
 * <p>
 * 解释：endWord "cog" 不在字典 wordList 中，所以不存在符合要求的转换序列。
 * <p>
 * 提示：
 * <p>
 * 1 <= beginWord.length <= 7
 * <p>
 * endWord.length == beginWord.length
 * <p>
 * 1 <= wordList.length <= 5000
 * <p>
 * wordList[i].length == beginWord.length
 * <p>
 * beginWord、endWord 和 wordList[i] 由小写英文字母组成
 * <p>
 * beginWord != endWord
 * <p>
 * wordList 中的所有单词 互不相同
 * <p>
 * Related Topics 广度优先搜索 哈希表 字符串 回溯 👍 510 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode126Test {
    private boolean[] visited;
    private int minLayer;

    private static class Input {
        String beginWord;
        String endWord;
        List<String> wordList;
        List<List<String>> excpected;

        public Input(String beginWord, String endWord, List<String> wordList, List<List<String>> excpected) {
            this.beginWord = beginWord;
            this.endWord = endWord;
            this.wordList = wordList;
            this.excpected = excpected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> rawInput = (List<Map<String, Object>>) CaseReader.read("leetcode_cases/leetcode126_cases.json");
        if (null != rawInput) {
            for (Map<String, Object> map : rawInput) {
                @SuppressWarnings("unchecked")
                Input input = new Input(map.get("beginWord").toString(), map.get("endWord").toString(),
                        (List<String>) map.get("wordList"),
                        (List<List<String>>) map.get("excpected")
                );
                inputs.add(input);
            }
        }
        return inputs;
    }

    private void solve(List<List<String>> ans, List<String> wordList, List<String> one, String beginWord, String endWord, int k) {
        if (isOnlyOneCharDifferenct(beginWord, endWord)) {
            one.add(endWord);
            if (one.size() < minLayer) {
                minLayer = one.size();
                ans.clear();
                ans.add(new ArrayList<>(one));
            } else if (one.size() == minLayer) {
                ans.add(new ArrayList<>(one));
            }
            one.remove(one.size() - 1);
            return;
        }
        if (k >= minLayer) {
            return;
        }
        for (int i = 0; i < wordList.size(); i++) {
            if (!visited[i]) {
                String w = wordList.get(i);
                if (isOnlyOneCharDifferenct(beginWord, w)) {
                    visited[i] = true;
                    one.add(w);
                    solve(ans, wordList, one, one.get(one.size() - 1), endWord, k + 1);
                    one.remove(one.size() - 1);
                    visited[i] = false;
                }
            }
        }
    }

    private List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> ans = new ArrayList<>();
        if (null != wordList) {
            if (wordList.contains(endWord)) {
                if (wordList.size() == 1) {
                    if (isOnlyOneCharDifferenct(beginWord, endWord)) {
                        ans.add(Arrays.asList(beginWord, endWord));
                    }
                } else {
                    List<String> one = new ArrayList<>();
                    visited = new boolean[wordList.size()];
                    minLayer = Integer.MAX_VALUE;
                    one.add(beginWord);
                    solve(ans, wordList, one, beginWord, endWord, 0);
                }
            }
        }
        return ans;
    }

    private boolean isOnlyOneCharDifferenct(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        } else {
            int c = 0;
            for (int i = 0; i < a.length(); i++) {
                if (a.charAt(i) != b.charAt(i)) {
                    c += 1;
                }
                if (c == 2) {
                    return false;
                }
            }
            return c == 1;
        }
    }

    private List<List<String>> solution(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> ans = new ArrayList<>();
        if (wordList.contains(endWord)) {
            if (wordList.size() == 1) {
                if (isOnlyOneCharDifferenct(beginWord, endWord)) {
                    ans.add(Arrays.asList(beginWord, endWord));
                }
            } else {
                int wordSize = wordList.size();
                visited = new boolean[wordSize];
                Deque<String> q = new ArrayDeque<>();
                q.push(beginWord);
                Deque<String> one = new ArrayDeque<>();
                one.addLast(beginWord);
                while (!q.isEmpty()) {
                    String s = q.pop();
                    one.addLast(s);
                    for (String w : wordList) {
                        if (!one.contains(w)) {
                            if (isOnlyOneCharDifferenct(one.getLast(), w)) {
                                if (w.equals(endWord)) {
                                    one.addLast(w);
                                    ans.add(new ArrayList<>(one));
                                    one.removeLast();
                                    break;
                                } else {
                                    q.push(w);
                                }
                            }
                        }
                    }
                    one.removeLast();
                }
            }
        }
        return ans;
    }

    private void solution1(List<List<String>> ans, List<String> wordList, Deque<String> one, String endWord, int layer) {
        if (layer > minLayer) {
            return;
        }
        Deque<String> m = new ArrayDeque<>();
        for (String w : wordList) {
            if (!one.contains(w)) {
                String last = one.getLast();
                if (isOnlyOneCharDifferenct(last, w)) {
                    if (w.equals(endWord)) {
                        one.addLast(w);
                        ans.add(new ArrayList<>(one));
                        one.removeLast();
                        minLayer = layer;
                        break;
                    } else {
                        if (layer < minLayer) {
                            m.push(w);
                        }
                    }
                }
            }
        }
        int newLayer = layer + 1;
        while (!m.isEmpty()) {
            one.addLast(m.pop());
            solution1(ans, wordList, one, endWord, newLayer);
            one.removeLast();
        }
    }

    private List<List<String>> breadthFirst(String beginWord, String endWord, List<String> wordList) {
        List<List<String>> ans = new ArrayList<>();
        HashSet<String> wordHashSet = new HashSet<>(wordList);
        if (wordHashSet.contains(endWord)) {
            final char a = 'a';
            final char z = 'z';
            wordHashSet.remove(beginWord);
            Map<String, Integer> layers = new HashMap<>();
            Map<String, List<String>> from = new HashMap<>();
            layers.put(beginWord, 0);
            boolean found = false;
            int layer = 1;
            int wordLength = beginWord.length();
            Deque<String> q = new ArrayDeque<>();
            q.addFirst(beginWord);
            while (!q.isEmpty()) {
                int qSize = q.size();
                for (int i = 0; i < qSize; i++) {
                    String word = q.pollLast();
                    if (null != word) {
                        char[] wordCharArray = word.toCharArray();
                        for (int j = 0; j < wordLength; j++) {
                            char originChar = wordCharArray[j];
                            for (char c = a; c <= z; c++) {
                                wordCharArray[j] = c;
                                String nxtWord = String.valueOf(wordCharArray);
                                if (layers.containsKey(nxtWord) && layer == layers.get(nxtWord)) {
                                    from.get(nxtWord).add(word);
                                }
                                if (wordHashSet.contains(nxtWord)) {
                                    wordHashSet.remove(nxtWord);
                                    q.addFirst(nxtWord);
                                    from.putIfAbsent(nxtWord, new ArrayList<>());
                                    from.get(nxtWord).add(word);
                                    layers.put(nxtWord, layer);
                                    if (nxtWord.equals(endWord)) {
                                        found = true;
                                    }
                                }
                            }
                            wordCharArray[j] = originChar;
                        }
                    }
                }
                layer += 1;
                if (found) {
                    break;
                }
            }
            if (found) {
                Deque<String> path = new ArrayDeque<>();
                path.add(endWord);
                dfs(ans, from, path, beginWord, endWord);
            }
        }
        return ans;
    }

    private void dfs(List<List<String>> ans, Map<String, List<String>> from, Deque<String> path, String beginWord, String cur) {
        if (cur.equals(beginWord)) {
            ans.add(new ArrayList<>(path));
            return;
        }
        for (String precursor : from.get(cur)) {
            path.addFirst(precursor);
            dfs(ans, from, path, beginWord, precursor);
            path.removeFirst();
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void breadthFirstTest(Input input) {
        String beginWord = input.beginWord;
        String endWord = input.endWord;
        List<String> wordList = input.wordList;
        List<List<String>> excpected = input.excpected;
        List<List<String>> ans = breadthFirst(beginWord, endWord, wordList);
        Assertions.assertEquals(excpected, ans);
    }

    public void dequeTest() {
        Deque<String> d = new ArrayDeque<>();
        d.add("add");
        // head, at index 0
        d.addFirst("addFirst");
        // tail, at index size - 1
        d.addLast("addLast");
        // = addLast
        d.add("c");
        d.add("1");
        d.add("2");
        d.add("d");
        d.add("e");
        d.add("f");
        // = addFirst, at index 0
        d.push("push");
        // = removeFirst
        d.pop();
        // = removeFirst
        d.remove();
        // remove tail
        d.removeLast();
        // remove head
        d.removeFirst();
        d.poll(); // = removeFirst
        d.pollFirst(); // = removeFirst
        d.pollLast(); // = removeLast

        System.out.println(d);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void solution1Test(Input input) {
        String beginWord = input.beginWord;
        String endWord = input.endWord;
        List<String> wordList = input.wordList;
        List<List<String>> excpected = input.excpected;
        List<List<String>> ans = new ArrayList<>();
        Deque<String> one = new ArrayDeque<>();
        visited = new boolean[wordList.size()];
        minLayer = Integer.MAX_VALUE;
        one.addLast(beginWord);
        solution1(ans, wordList, one, endWord, 1);
        System.out.println(ans);
        Assertions.assertEquals(excpected, ans);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void solutionTest(Input input) {
        String beginWord = input.beginWord;
        String endWord = input.endWord;
        List<String> wordList = input.wordList;
        List<List<String>> excpected = input.excpected;

        List<List<String>> ladders = solution(beginWord, endWord, wordList);
        Assertions.assertEquals(excpected, ladders);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        String beginWord = input.beginWord;
        String endWord = input.endWord;
        List<String> wordList = input.wordList;
        List<List<String>> excpected = input.excpected;
        List<List<String>> ladders = findLadders(beginWord, endWord, wordList);
        Assertions.assertEquals(excpected, ladders);
    }
}
