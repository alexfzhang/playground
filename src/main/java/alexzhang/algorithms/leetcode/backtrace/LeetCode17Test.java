package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
 * <p>
 * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
 * <p>
 * 示例 1：
 * <p>
 * 输入：digits = "23"
 * <p>
 * 输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
 * <p>
 * 示例 2：
 * <p>
 * 输入：digits = ""
 * <p>
 * 输出：[]
 * <p>
 * 示例 3：
 * <p>
 * 输入：digits = "2"
 * <p>
 * 输出：["a","b","c"]
 * <p>
 * 提示：
 * <p>
 * 0 <= digits.length <= 4
 * <p>
 * digits[i] 是范围 ['2', '9'] 的一个数字。
 * <p>
 * Related Topics 哈希表 字符串 回溯 👍 1571 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode17Test {
    private List<String> ans;
    private StringBuilder sb;

    private static class Input {
        String digits;
        List<String> expected;

        public Input(String digits, List<String> expected) {
            this.digits = digits;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input("23", Arrays.asList("ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"));
        inputs.add(input);

        input = new Input("", new ArrayList<>());
        inputs.add(input);

        input = new Input("2", Arrays.asList("a", "b", "c"));
        inputs.add(input);

        return inputs;
    }

    private void solution1(String[] digits, int k) {
        if (k == digits.length) {
            ans.add(sb.toString());
            return;
        }
        String alphabets = digits[k];
        for (int i = 0; i < alphabets.length(); i++) {
            sb.append(alphabets.charAt(i));
            solution1(digits, k + 1);
            sb.deleteCharAt(sb.length() - 1);
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        String digits = input.digits;
        List<String> expected = input.expected;
        ans = new ArrayList<>();
        if (digits.length() > 0) {
            sb = new StringBuilder();
            String[] alphabetsArray = new String[digits.length()];
            for (int i = 0; i < digits.length(); i++) {
                char c = digits.charAt(i);
                switch (c) {
                    case '2':
                        alphabetsArray[i] = "abc";
                        break;
                    case '3':
                        alphabetsArray[i] = "def";
                        break;
                    case '4':
                        alphabetsArray[i] = "ghi";
                        break;
                    case '5':
                        alphabetsArray[i] = "jkl";
                        break;
                    case '6':
                        alphabetsArray[i] = "mno";
                        break;
                    case '7':
                        alphabetsArray[i] = "pqrs";
                        break;
                    case '8':
                        alphabetsArray[i] = "tuv";
                        break;
                    case '9':
                        alphabetsArray[i] = "wxyz";
                        break;
                    default:
                        alphabetsArray[i] = "";
                }
            }
            solution1(alphabetsArray, 0);
            System.out.println(ans);
            Assertions.assertEquals(expected, ans);
        }
    }
}
