package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode24Test {
    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append(",");
                a = a.next;
            }
            s.setLength(s.length() - 1);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l;
        private final ListNode expected;

        public Input(ListNode l, ListNode expected) {
            this.l = l;
            this.expected = expected;
        }

        public ListNode getL() {
            return l;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create()),
                new Input(ListNode.create(1), ListNode.create(1)),
                new Input(ListNode.create(1, 2), ListNode.create(2, 1)),
                new Input(ListNode.create(1, 2, 3, 4), ListNode.create(2, 1, 4, 3)),
                new Input(ListNode.create(1, 2, 3, 4, 5), ListNode.create(2, 1, 4, 3, 5)),
                new Input(ListNode.create(2, 5, 3, 4, 6, 2, 2), ListNode.create(5, 2, 4, 3, 2, 6, 2))
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
        ListNode head = input.getL();
        ListNode expected = input.getExpected();
        if (null != head && null != head.next) {
            ListNode a = head;
            ListNode b = head.next;
            head = b;
            ListNode h = b.next;
            ListNode temp = null;
            while (null != h && null != h.next) {
                if (null != temp) {
                    temp.next = b;
                }
                a.next = b.next;
                b.next = a;
                temp = a;
                a = h;
                b = h.next;
                h = b.next;
            }
            a.next = b.next;
            b.next = a;
            if (null != temp) {
                temp.next = b;
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
