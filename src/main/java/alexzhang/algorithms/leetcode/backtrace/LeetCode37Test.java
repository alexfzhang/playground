package alexzhang.algorithms.leetcode.backtrace;

import alexzhang.algorithms.leetcode.util.CaseReader;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 编写一个程序，通过填充空格来解决数独问题。
 * <p>
 * 数独的解法需 遵循如下规则：
 * <p>
 * <p>
 * 数字 1-9 在每一行只能出现一次。
 * 数字 1-9 在每一列只能出现一次。
 * 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）
 * <p>
 * 数独部分空格内已填入了数字，空白格用 '.' 表示。
 * <p>
 * 示例：
 * <p>
 * 输入：board = [["5","3",".",".","7",".",".",".","."],["6",".",".","1","9","5",".
 * ",".","."],[".","9","8",".",".",".",".","6","."],["8",".",".",".","6",".",".",".
 * ","3"],["4",".",".","8",".","3",".",".","1"],["7",".",".",".","2",".",".",".","6
 * "],[".","6",".",".",".",".","2","8","."],[".",".",".","4","1","9",".",".","5"],[
 * ".",".",".",".","8",".",".","7","9"]]
 * 输出：[["5","3","4","6","7","8","9","1","2"],["6","7","2","1","9","5","3","4","8
 * "],["1","9","8","3","4","2","5","6","7"],["8","5","9","7","6","1","4","2","3"],[
 * "4","2","6","8","5","3","7","9","1"],["7","1","3","9","2","4","8","5","6"],["9",
 * "6","1","5","3","7","2","8","4"],["2","8","7","4","1","9","6","3","5"],["3","4",
 * "5","2","8","6","1","7","9"]]
 * 解释：输入的数独如上图所示，唯一有效的解决方案如下所示：
 * <p>
 * 提示：
 * <p>
 * board.length == 9
 * board[i].length == 9
 * board[i][j] 是一位数字或者 '.'
 * 题目数据 保证 输入数独仅有一个解
 * <p>
 * Related Topics 数组 回溯 矩阵 👍 952 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode37Test {

    private final boolean[][] row = new boolean[9][9];
    private final boolean[][] col = new boolean[9][9];
    private final boolean[][][] block = new boolean[3][3][9];
    private final List<int[]> empty = new ArrayList<>();
    private boolean solved = false;

    private static class Constants {
        static final int NINE = 9;
        static final char CHAR_DOT = '.';
    }

    private void solve(char[][] board, int pos) {
        if (pos == empty.size()) {
            solved = true;
            return;
        }
        int[] p = empty.get(pos);
        int x = p[0];
        int y = p[1];
        for (int digit = 0; digit < Constants.NINE && !solved; digit++) {
            if (!row[x][digit] && !col[y][digit] && !block[x / 3][y / 3][digit]) {
                row[x][digit] = col[y][digit] = block[x / 3][y / 3][digit] = true;
                board[x][y] = (char) (digit + '0' + 1);
                solve(board, pos + 1);
                row[x][digit] = col[y][digit] = block[x / 3][y / 3][digit] = false;
            }
        }
    }

    private static class Input {
        char[][] board;

        public Input(char[][] board) {
            this.board = board;
        }

    }

    private static Input[] createInput() {
        @SuppressWarnings("unchecked")
        List<List<List<String>>> rawInput = (List<List<List<String>>>) CaseReader.read("leetcode_cases/leetcode37_cases.json");
        if (null != rawInput) {
            Input[] inputs = new Input[rawInput.size()];
            int k = 0;
            for (List<List<String>> cs : rawInput) {
                char[][] in = new char[9][9];
                for (int i = 0; i < cs.size(); i++) {
                    List<String> row = cs.get(i);
                    for (int j = 0; j < row.size(); j++) {
                        String s = row.get(j);
                        in[i][j] = s.charAt(0);
                    }
                }
                inputs[k++] = new Input(in);
            }
            return inputs;
        }
        return new Input[0];
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void solveSudoku(Input input) {
        char[][] board = input.board;
        for (int i = 0; i < Constants.NINE; i++) {
            for (int j = 0; j < Constants.NINE; j++) {
                if (board[i][j] == Constants.CHAR_DOT) {
                    empty.add(new int[]{i, j});
                } else {
                    int digit = board[i][j] - '0' - 1;
                    row[i][digit] = col[j][digit] = block[i / 3][j / 3][digit] = true;
                }
            }
        }
        solve(board, 0);
        if (solved) {
            System.out.println(Arrays.deepToString(board));
        }
    }
}
