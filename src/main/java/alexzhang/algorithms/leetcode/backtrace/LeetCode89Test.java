package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 这种方法基于格雷码是反射码的事实，利用递归的如下规则来构造：
 * <p>
 * 1位格雷码有两个码字
 * <ol>
 *  <li>(n+1)位格雷码中的后2n个码字等于n位格雷码的码字，按逆序书写，加前缀1 [4]</li>
 *  <li>(n+1)位格雷码中的前2n个码字等于n位格雷码的码字，按顺序书写，加前缀0</li>
 *  <li>n+1位格雷码的集合 = n位格雷码集合(顺序)加前缀0 + n位格雷码集合(逆序)加前缀1</li>
 * </ol>
 * <p>
 * 格雷编码是一个二进制数字系统，在该系统中，两个连续的数值仅有一个位数的差异。
 * <p>
 * 给定一个代表编码总位数的非负整数 n，打印其格雷编码序列。即使有多个不同答案，你也只需要返回其中一种。
 * <p>
 * 格雷编码序列必须以 0 开头。
 * <p>
 * 示例 1:
 * <p>
 * 输入:2
 * <p>
 * 输出:[0,1,3,2]
 * <p>
 * 解释:
 * <p>
 * 00 - 0
 * <p>
 * 01 - 1
 * <p>
 * 11 - 3
 * <p>
 * 10 - 2
 * <p>
 * 对于给定的n，其格雷编码序列并不唯一。
 * <p>
 * 例如，[0,2,3,1]也是一个有效的格雷编码序列。
 * <p>
 * 00 - 0
 * <p>
 * 10 - 2
 * <p>
 * 11 - 3
 * <p>
 * 01 - 1
 * <p>
 * 示例 2:
 * <p>
 * 输入:0
 * <p>
 * 输出:[0]
 * <p>
 * 解释: 我们定义格雷编码序列必须以 0 开头。
 * <p>
 * 给定编码总位数为 n 的格雷编码序列，其长度为 2ⁿ。当 n = 0 时，长度为 2⁰ = 1。
 * <p>
 * 因此，当 n = 0 时，其格雷编码序列为 [0]。
 * <p>
 * Related Topics 位运算 数学 回溯 👍 341 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode89Test {

    private boolean[] visited;
    private List<List<Integer>> ans;

    private static class Input {
        int n;
        List<Integer> expected;

        public Input(int n, List<Integer> expected) {
            this.n = n;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(3, Arrays.asList(0, 1, 3, 2, 6, 7, 5, 4));
        inputs.add(input);

        input = new Input(2, Arrays.asList(0, 1, 3, 2));
        inputs.add(input);

        input = new Input(0, Collections.singletonList(0));
        inputs.add(input);

        return inputs;
    }

    private boolean validate(int a, int b) {
        if (a == 0) {
            if (b != 0) {
                return Integer.bitCount(b) == 1;
            } else {
                return false;
            }
        } else {
            if (b != 0) {
                return ((a | b) == b) || ((a | b) == a);
            } else {
                return Integer.bitCount(a) == 1;
            }
        }
    }

    private void solve(List<Integer> gc, int max) {
        if (gc.size() == max) {
            ans.add(new ArrayList<>(gc));
            return;
        }
        for (int i = 1; i < max; i++) {
            if (!visited[i]) {
                if (validate(gc.get(gc.size() - 1), i)) {
                    visited[i] = true;
                    gc.add(i);
                    solve(gc, max);
                    visited[i] = false;
                    gc.remove(gc.size() - 1);
                }
            }
        }
    }

    private List<Integer> grayCodeBacktrace(int n) {
        ans = new ArrayList<>();
        List<Integer> gc = new ArrayList<>();
        int max = (int) Math.pow(2, n);
        visited = new boolean[max];
        gc.add(0);
        visited[0] = true;
        solve(gc, max);
        return gc;
    }

    private List<Integer> grayCodeNonRecursive(int n) {
        List<Integer> ans = new ArrayList<>();
        ans.add(0);
        int head = 1;
        for (int i = 0; i < n; i++) {
            for (int j = ans.size() - 1; j >= 0; j--) {
                ans.add(head + ans.get(j));
            }
            head <<= 1;
        }
        return ans;
    }

    private void grayCodeRecursive(List<Integer> ans, int n) {
        if (n == 0) {
            ans.add(0);
            return;
        }
        grayCodeRecursive(ans, n - 1);
        int head = 1 << (n - 1);
        for (int i = ans.size() - 1; i >= 0; i--) {
            ans.add(head + ans.get(i));
        }
    }

    /**
     * 异或转换
     * <p>
     * 二进制码→格雷码（编码）：
     * <p>
     * 此方法从对应的n位二进制码字中直接得到n位格雷码码字，步骤如下：
     * <p>
     * 对n位二进制的码字，从右到左，以0到n-1编号
     * <p>
     * 如果二进制码字的第i位和i+1位相同，则对应的格雷码的第i位为0，否则为1（当i+1=n时，二进制码字的第n位被认为是0，即第n-1位不变） [4]
     * <p>
     * 公式表示： （G：格雷码，B：二进制码）
     * <p>
     * 例如：二进制码0101，为4位数，所以其所转为之格雷码也必为4位数，因此可取转成之二进位码第五位为0，即0 b3 b2 b1 b0。
     * <p>
     * 0 xor 0=0，所以g3=0
     * <p>
     * 0 xor 1=1，所以g2=1
     * <p>
     * 1 xor 0=1，所以g1=1
     * <p>
     * 0 xor 1=1，所以g0=1
     * <p>
     * 因此所转换为之格雷码为0111
     * <p>
     * 格雷码→二进制码（解码）：
     * <p>
     * 从左边第二位起，将每位与左边一位解码后的值异或，作为该位解码后的值（最左边一位依然不变）。依次异或，直到最低位。依次异或转换后的值（二进制数）就是格雷码转换后二进制码的值。
     * <p>
     * 公式表示： （G：格雷码，B：二进制码）
     * <p>
     * 原码：p[n:0]；格雷码：c[n:0](n∈N）；编码：c=G(p）；解码：p=F(c）；
     * <p>
     * 书写时按从左向右标号依次减小，即MSB->LSB，编解码也按此顺序进行
     *
     * @param n n
     * @return gray code
     */
    private List<Integer> grayCodeXor(int n) {
        List<Integer> ans = new ArrayList<>();
        final int base = 1;
        for (int i = 0; i < (base << n); i++) {
            ans.add(i ^ (i >> 1));
        }
        return ans;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void backtraceTest(Input input) {
        int n = input.n;
        List<Integer> expected = input.expected;
        List<Integer> gc = grayCodeBacktrace(n);
        for (int g : gc) {
            System.out.println(Integer.toBinaryString(g));
        }
        System.out.println(ans);
        Assertions.assertEquals(expected, gc);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void grayCodeNonRecursiveTest(Input input) {
        int n = input.n;
        List<Integer> expected = input.expected;
        List<Integer> gc = grayCodeNonRecursive(n);
        Assertions.assertEquals(expected, gc);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void grayCodeRecursiveTest(Input input) {
        int n = input.n;
        List<Integer> expected = input.expected;
        List<Integer> gc = new ArrayList<>();
        grayCodeRecursive(gc, n);
        Assertions.assertEquals(expected, gc);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void grayCodeXorTest(Input input) {
        int n = input.n;
        List<Integer> expected = input.expected;
        List<Integer> gc = grayCodeXor(n);
        Assertions.assertEquals(expected, gc);
    }
}
