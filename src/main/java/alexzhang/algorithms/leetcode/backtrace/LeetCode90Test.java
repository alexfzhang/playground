package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。
 * <p>
 * 解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
 * <p>
 * 示例 1：
 * <p>
 * <p>
 * 输入：nums = [1,2,2]
 * <p>
 * 输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
 * <p>
 * 示例 2：
 * <p>
 * 输入：nums = [0]
 * <p>
 * 输出：[[],[0]]
 * <p>
 * 提示：
 * <p>
 * 1 <= nums.length <= 10
 * <p>
 * -10 <= nums[i] <= 10
 * <p>
 * Related Topics 位运算 数组 回溯 👍 675 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode90Test {
    List<List<Integer>> ans;
    List<Integer> subset;

    private static class Input {
        int[] nums;
        List<List<Integer>> expected;

        public Input(int[] nums, List<List<Integer>> expected) {
            this.nums = nums;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(new int[]{1, 2, 2}, Arrays.asList(
                new ArrayList<>(),
                Collections.singletonList(1),
                Collections.singletonList(2),
                Arrays.asList(1, 2),
                Arrays.asList(2, 2),
                Arrays.asList(1, 2, 2)
        ));
        inputs.add(input);

        input = new Input(new int[]{4, 4, 4, 1, 4}, Arrays.asList(
                new ArrayList<>(),
                Collections.singletonList(1),
                Collections.singletonList(4),
                Arrays.asList(1, 4),
                Arrays.asList(4, 4),
                Arrays.asList(1, 4, 4),
                Arrays.asList(4, 4, 4),
                Arrays.asList(1, 4, 4, 4),
                Arrays.asList(4, 4, 4, 4),
                Arrays.asList(1, 4, 4, 4, 4)
        ));
        inputs.add(input);

        input = new Input(new int[]{0}, Arrays.asList(
                new ArrayList<>(),
                Collections.singletonList(0)
        ));
        inputs.add(input);

        return inputs;
    }

    private void solution1(int[] nums, int m, int pos, int k) {
        if (k == m) {
            ans.add(new ArrayList<>(subset));
            return;
        }
        for (int i = pos; i < nums.length; i++) {
            if (i > pos && nums[i] == nums[i - 1]) {
                continue;
            }
            subset.add(nums[i]);
            solution1(nums, m, i + 1, k + 1);
            subset.remove(subset.size() - 1);
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] nums = input.nums;
        List<List<Integer>> expected = input.expected;

        ans = new ArrayList<>();
        if (nums.length > 0) {
            subset = new ArrayList<>();
            Arrays.sort(nums);
            for (int m = 0; m <= nums.length; m++) {
                solution1(nums, m, 0, 0);
            }
        }

        System.out.println(ans);
        Assertions.assertEquals(expected, ans);
    }
}
