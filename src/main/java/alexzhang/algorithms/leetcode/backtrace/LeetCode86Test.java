//给你一个链表的头节点 head 和一个特定值 x ，请你对链表进行分隔，使得所有 小于 x 的节点都出现在 大于或等于 x 的节点之前。
//
// 你应当 保留 两个分区中每个节点的初始相对位置。
//
//
//
// 示例 1：
//
//
//输入：head = [1,4,3,2,5,2], x = 3
//输出：[1,2,2,4,3,5]
//
//
// 示例 2：
//
//
//输入：head = [2,1], x = 2
//输出：[1,2]
//
//
//
//
// 提示：
//
//
// 链表中节点的数目在范围 [0, 200] 内
// -100 <= Node.val <= 100
// -200 <= x <= 200
//
// Related Topics 链表 双指针
// 👍 429 👎 0

package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode86Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append("->");
                a = a.next;
            }
            s.setLength(s.length() - 2);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode l;
        private final ListNode expected;
        private final int val;

        public Input(ListNode l, ListNode expected, int val) {
            this.l = l;
            this.expected = expected;
            this.val = val;
        }

        public ListNode getL() {
            return l;
        }

        public ListNode getExpected() {
            return expected;
        }

        public int getVal() {
            return val;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), ListNode.create(), 2),
                new Input(ListNode.create(1), ListNode.create(1), 2),
                new Input(ListNode.create(1, 1, 1), ListNode.create(1, 1, 1), 3),
                new Input(ListNode.create(3, 3, 3), ListNode.create(3, 3, 3), 3),
                new Input(ListNode.create(3, 3, 1), ListNode.create(1, 3, 3), 3),
                new Input(ListNode.create(4, 5, 6), ListNode.create(4, 5, 6), 3),
                new Input(ListNode.create(1, 4, 3, 2, 5, 3), ListNode.create(1, 2, 4, 3, 5, 3), 3),
                new Input(ListNode.create(1, 4, 3, 2, 5, 2), ListNode.create(1, 2, 2, 4, 3, 5), 3),
                new Input(ListNode.create(2, 1), ListNode.create(1, 2), 2)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
        ListNode head = input.getL();
        ListNode expected = input.getExpected();
        if (null != head && null != head.next) {
            int x = input.getVal();
            ListNode small = null;
            ListNode smallTail = null;
            ListNode p = head;
            ListNode n = head;
            while (null != p) {
                if (p.val < x) {
                    if (p == head) {
                        head = p.next;
                    } else {
                        n.next = p.next;
                    }
                    if (null == small) {
                        small = p;
                        smallTail = small;
                    } else {
                        smallTail.next = p;
                        smallTail = smallTail.next;
                    }
                } else {
                    n = p;
                }
                p = p.next;
            }
            if (null != smallTail) {
                smallTail.next = head;
                head = small;
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
