//给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链
//表节点，返回 反转后的链表 。
//
//
// 示例 1：
//
//
//输入：head = [1,2,3,4,5], left = 2, right = 4
//输出：[1,4,3,2,5]
//
//
// 示例 2：
//
//
//输入：head = [5], left = 1, right = 1
//输出：[5]
//
//
//
//
// 提示：
//
//
// 链表中节点数目为 n
// 1 <= n <= 500
// -500 <= Node.val <= 500
// 1 <= left <= right <= n
//
//
//
//
// 进阶： 你可以使用一趟扫描完成反转吗？
// Related Topics 链表
// 👍 966 👎 0

package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Objects;

/**
 * @author alexzhang
 */
public class LeetCode92Test {

    private static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        static ListNode create(int... numbers) {
            ListNode h;
            if (numbers.length > 0) {
                h = new ListNode(numbers[0]);
                ListNode p = h;
                for (int i = 1; i < numbers.length; i++) {
                    p.next = new ListNode(numbers[i]);
                    p = p.next;
                }
            } else {
                h = null;
            }
            return h;
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ListNode listNode = (ListNode) o;
            ListNode b = this;
            while (null != b && null != listNode) {
                if (b.val != listNode.val) {
                    return false;
                }
                b = b.next;
                listNode = listNode.next;
            }
            return null == b && null == listNode;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }

        @Override
        public String toString() {
            ListNode a = this;
            StringBuilder s = new StringBuilder();
            while (null != a) {
                s.append(a.val).append("->");
                a = a.next;
            }
            s.setLength(s.length() - 2);
            return s.toString();
        }
    }

    private static class Input {
        private final ListNode head;
        private final int left;
        private final int right;
        private final ListNode expected;

        public Input(ListNode head, int left, int right, ListNode expected) {
            this.head = head;
            this.left = left;
            this.right = right;
            this.expected = expected;
        }

        public ListNode getHead() {
            return head;
        }

        public int getLeft() {
            return left;
        }

        public int getRight() {
            return right;
        }

        public ListNode getExpected() {
            return expected;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(ListNode.create(), 1, 2, ListNode.create()),
                new Input(ListNode.create(1), 1, 2, ListNode.create(1)),
                new Input(ListNode.create(1, 2), 1, 2, ListNode.create(2, 1)),
                new Input(ListNode.create(1, 2, 3, 4, 5), 2, 4, ListNode.create(1, 4, 3, 2, 5))
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void reverseBetween(Input input) {
        ListNode head = input.getHead();
        ListNode expected = input.getExpected();
        if (null != head && null != head.next) {
            int left = input.getLeft();
            int right = input.getRight();
            ListNode a = head;
            ListNode b = head.next;
            ListNode h = head;
            ListNode p = head;
            int count = 1;
            while (null != b) {
                if (count >= left && count < right) {
                    // reverse start point
                    a.next = b.next;
                    b.next = h;
                    h = b;
                    // avoid loop
                    if(p != h.next) {
                        p.next = h;
                    } else {
                        head = h;
                        p = h;
                    }
                    b = a.next;
                } else if (count == right) {
                    // reverse stop point
                    break;
                } else {
                    p = a;
                    a = a.next;
                    h = h.next;
                    b = b.next;
                }
                count += 1;
            }
        }
        Assertions.assertEquals(expected, head);
    }
}
