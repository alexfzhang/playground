package alexzhang.algorithms.leetcode.backtrace;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [1,2,3]
 * <p>
 * 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
 * <p>
 * 示例 2：
 * <p>
 * 输入：nums = [0,1]
 * <p>
 * 输出：[[0,1],[1,0]]
 * <p>
 * 示例 3：
 * <p>
 * 输入：nums = [1]
 * <p>
 * 输出：[[1]]
 * <p>
 * 提示：
 * <p>
 * 1 <= nums.length <= 6
 * <p>
 * -10 <= nums[i] <= 10
 * <p>
 * nums 中的所有整数 互不相同
 * <p>
 * Related Topics 数组 回溯 👍 1586 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode46Test {
    private static class Input {
        final int[] nums;
        final List<List<Integer>> expected;

        public Input(List<List<Integer>> expected, int... numbers) {
            this.expected = expected;
            nums = numbers;
        }
    }

    private static List<Input> createInput() {
        List<Input> input = new ArrayList<>();
        List<List<Integer>> ex = new ArrayList<>();
        ex.add(Arrays.asList(1, 2, 3));
        ex.add(Arrays.asList(1, 3, 2));
        ex.add(Arrays.asList(2, 1, 3));
        ex.add(Arrays.asList(2, 3, 1));
        ex.add(Arrays.asList(3, 1, 2));
        ex.add(Arrays.asList(3, 2, 1));
        input.add(new Input(ex, 1, 2, 3));
        return input;
    }

    private void swap(int[] nums, int a, int b) {
        if (a != b) {
            int t = nums[a];
            nums[a] = nums[b];
            nums[b] = t;
        }
    }

    private void permutation(List<List<Integer>> rs, int[] nums, int k, int m) {
        if (k == m) {
            rs.add(Arrays.stream(nums).boxed().collect(Collectors.toList()));
            return;
        }
        for (int i = k; i <= m; i++) {
            swap(nums, k, i);
            permutation(rs, nums, k + 1, m);
            swap(nums, k, i);
        }
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void permute(Input input) {
        int[] nums = input.nums;
        List<List<Integer>> rs = new ArrayList<>();
        permutation(rs, nums, 0, nums.length - 1);
        for (List<Integer> r : rs) {
            System.out.println(r);
        }
    }
}
