package alexzhang.algorithms.leetcode.sort;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.ArrayList;

/**
 * 给你一个整数数组 nums ，其中总是存在 唯一的 一个最大整数 。
 * <p>
 * 请你找出数组中的最大元素并检查它是否 至少是数组中每个其他数字的两倍 。如果是，则返回 最大元素的下标 ，否则返回 -1 。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [3,6,1,0]
 * 输出：1
 * 解释：6 是最大的整数，对于数组中的其他整数，6 大于数组中其他元素的两倍。6 的下标是 1 ，所以返回 1 。
 * 示例 2：
 * <p>
 * 输入：nums = [1,2,3,4]
 * 输出：-1
 * 解释：4 没有超过 3 的两倍大，所以返回 -1 。
 * 示例 3：
 * <p>
 * 输入：nums = [1]
 * 输出：0
 * 解释：因为不存在其他数字，所以认为现有数字 1 至少是其他数字的两倍。
 * 提示：
 * <p>
 * 1 <= nums.length <= 50
 * 0 <= nums[i] <= 100
 * nums 中的最大元素是唯一的
 * Related Topics
 * 数组
 * 排序
 * <p>
 * 👍 135
 * 👎 0
 *
 * @author Alex F Zhang
 */
public class LeetCode747Test {
    private static class Input {
        int[] nums;
        int expected;

        public Input(int[] nums, int expected) {
            this.nums = nums;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input(new int[]{3, 6, 1, 0}, 1);
        inputs.add(input);

        input = new Input(new int[]{1, 2, 3, 4}, -1);
        inputs.add(input);

        input = new Input(new int[]{1}, 0);
        inputs.add(input);

        return inputs;
    }

    private int dominantIndex(int[] nums) {
        int maxIndex = -1;
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        final int two = 2;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max) {
                secondMax = max;
                max = nums[i];
                maxIndex = i;
            } else if (nums[i] > secondMax) {
                secondMax = nums[i];
            }
        }
        return (max >= secondMax * two) ? maxIndex : -1;
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void test(Input input) {
        int[] nums = input.nums;
        int expected = input.expected;
        int ans = dominantIndex(nums);
        Assertions.assertEquals(expected, ans);
    }
}
