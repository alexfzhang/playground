package alexzhang.algorithms.leetcode.util;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

/**
 * @author Alex F Zhang
 */
public class CaseReader {
    private static final String JSON_OBJ_PREFIX = "{";
    private static final String JSON_ARR_PREFIX = "[";
    public static Object read(String caseFile) {
        try(InputStream is = CaseReader.class.getClassLoader().getResourceAsStream(caseFile)) {
            if(null != is) {
                String s = IOUtils.toString(is);
                if(s.startsWith(JSON_OBJ_PREFIX)) {
                    return new JSONObject(s).toMap();
                }
                if(s.startsWith(JSON_ARR_PREFIX)) {
                    return new JSONArray(s).toList();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
