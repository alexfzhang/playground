package alexzhang.algorithms.leetcode.hashtable;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author Alex Zhang
 */
public class LeetCode3Test {
    private static class Input {
        String s;
        int expected;

        public Input(String s, int expected) {
            this.s = s;
            this.expected = expected;
        }
    }

    private static List<Input> createInput() {
        List<Input> inputs = new ArrayList<>();
        Input input;

        input = new Input("jbpnbwwd", 4);
        inputs.add(input);

        input = new Input(" ", 1);
        inputs.add(input);

        input = new Input("abcabcbb", 3);
        inputs.add(input);

        input = new Input("bbbbb", 1);
        inputs.add(input);

        input = new Input("pwwkew", 3);
        inputs.add(input);

        return inputs;
    }

    private int solution1(String s) {
        HashSet<Character> sub = new HashSet<>();
        int k = 0, ans = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (i != 0) {
                sub.remove(s.charAt(i - 1));
            }
            while (k < len && !sub.contains(s.charAt(k))) {
                sub.add(s.charAt(k));
                k += 1;
            }
            ans = Math.max(ans, k - i);
        }
        return ans;
    }

    private int solution2(String s) {
        HashMap<Character, Integer> map = new HashMap<>(s.length());
        int k = -1, ans = 0;
        int len = s.length();
        for (int i = 0; i < len; i++) {
            char c = s.charAt(i);
            if (map.containsKey(c) && map.get(c) > k) {
                k = map.get(c);
                map.put(c, i);
            } else {
                map.put(c, i);
                ans = Math.max(ans, i - k);
            }
        }
        return ans;
    }

    public int lengthOfLongestSubstring(String s) {
        return solution2(s);
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void lengthOfLongestSubstringTest(Input input) {
        String s = input.s;
        int expected = input.expected;
        int ans = lengthOfLongestSubstring(s);
        Assertions.assertEquals(expected, ans);
    }
}
