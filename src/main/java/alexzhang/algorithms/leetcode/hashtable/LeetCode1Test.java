package alexzhang.algorithms.leetcode.hashtable;

//给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
//
// 你可以按任意顺序返回答案。
//
//
//
// 示例 1：
//
//
//输入：nums = [2,7,11,15], target = 9
//输出：[0,1]
//解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
//
//
// 示例 2：
//
//
//输入：nums = [3,2,4], target = 6
//输出：[1,2]
//
//
// 示例 3：
//
//
//输入：nums = [3,3], target = 6
//输出：[0,1]
//
//
//
//
// 提示：
//
//
// 2 <= nums.length <= 104
// -109 <= nums[i] <= 109
// -109 <= target <= 109
// 只会存在一个有效答案
//
//
// 进阶：你可以想出一个时间复杂度小于 O(n2) 的算法吗？
// Related Topics 数组 哈希表
// 👍 11808 👎 0

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;

/**
 * @author alexzhang
 */
@Slf4j
public class LeetCode1Test {

    private static class Input {
        private final int[] head;
        private final int target;
        private final int[] expected;

        public Input(int[] head, int target, int[] expected) {
            this.head = head;
            this.target = target;
            this.expected = expected;
        }

        public int[] getHead() {
            return head;
        }

        public int[] getExpected() {
            return expected;
        }

        public int getTarget() {
            return target;
        }
    }

    private static Input[] createInput() {
        return new Input[]{
                new Input(new int[] {2,7,11,15}, 9, new int[] {0,1})
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
        int[] nums = input.getHead();
        int target = input.getTarget();
        int[] expected = input.getExpected();
        int[] rs = new int[2];
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        for(int i = 0;i< nums.length;i++) {
            if(map.containsKey(nums[i])) {
                rs[1] = i;
                rs[0] = map.get(nums[i]);
                break;
            } else {
                map.put(target - nums[i], i);
            }
        }
        Assertions.assertArrayEquals(expected, rs);
    }
}
