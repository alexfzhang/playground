package alexzhang.algorithms.leetcode.binarysearch;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums1 = [1,3], nums2 = [2]
 * <p>
 * 输出：2.00000
 * <p>
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * <p>
 * 示例 2：
 * <p>
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * <p>
 * 输出：2.50000
 * <p>
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 * <p>
 * 示例 3：
 * <p>
 * 输入：nums1 = [0,0], nums2 = [0,0]
 * <p>
 * 输出：0.00000
 * <p>
 * 示例 4：
 * <p>
 * 输入：nums1 = [], nums2 = [1]
 * <p>
 * 输出：1.00000
 * <p>
 * 示例 5：
 * <p>
 * 输入：nums1 = [2], nums2 = []
 * <p>
 * 输出：2.00000
 * <p>
 * 提示：
 * <p>
 * nums1.length == m
 * <p>
 * nums2.length == n
 * <p>
 * 0 <= m <= 1000
 * <p>
 * 0 <= n <= 1000
 * <p>
 * 1 <= m + n <= 2000
 * <p>
 * -106 <= nums1[i], nums2[i] <= 106
 * <p>
 * 进阶：你能设计一个时间复杂度为 O(log (m+n)) 的算法解决此问题吗？
 * <p>
 * Related Topics 数组 二分查找 分治
 * <p>
 * 👍 4250 👎 0
 *
 * @author alexzhang
 */
public class Leetcode4Test {

    public static class Input {
        private final int[] input1;
        private final int[] input2;
        private final double expectedResult;

        public Input(int[] input1, int[] input2, double expectedResult) {
            this.input1 = input1;
            this.input2 = input2;
            this.expectedResult = expectedResult;
        }

        public int[] getInput1() {
            return input1;
        }

        public int[] getInput2() {
            return input2;
        }

        public double getExpectedResult() {
            return expectedResult;
        }
    }

    public static Input[] createInput() {
        return new Input[]{new Input(new int[]{1, 3}, new int[]{2}, 2.0),
                new Input(new int[]{1, 2}, new int[]{3, 4}, 2.5),
                new Input(new int[]{0, 0}, new int[]{0, 0}, 0.0),
                new Input(new int[]{}, new int[]{1}, 1),
                new Input(new int[]{2}, new int[]{}, 2)
        };
    }

    @ParameterizedTest
    @MethodSource("createInput")
    void findMedianSortedArrays(Input input) {
        int[] numbers1 = input.getInput1();
        int[] numbers2 = input.getInput2();
        int len1 = numbers1.length;
        int len2 = numbers2.length;
        int[] merge = new int[len1 + len2];
        int i = 0, j = 0, k = 0;
        int m, n;
        while (i < len1 && j < len2) {
            m = numbers1[i];
            n = numbers2[j];
            if (m < n) {
                merge[k] = m;
                k += 1;
                i += 1;
            } else {
                merge[k] = n;
                k += 1;
                j += 1;
            }
        }
        if (i < len1) {
            while (i < len1) {
                merge[k] = numbers1[i];
                k += 1;
                i += 1;
            }
        }
        if (j < len2) {
            while (j < len2) {
                merge[k] = numbers2[j];
                k += 1;
                j += 1;
            }
        }
        int ml = merge.length;
        double rs;
        final int two = 2;
        if (ml % two == 0) {
            rs = (merge[ml / 2] + merge[(ml / 2) - 1]) / 2.0;
        } else {
            rs = merge[ml / 2];
        }
        assertEquals(rs, input.getExpectedResult());
    }

}
