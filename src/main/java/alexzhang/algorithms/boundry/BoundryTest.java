package alexzhang.algorithms.boundry;

//import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.junit.jupiter.api.Test;

/**
 * @author Alex F Zhang
 */
public class BoundryTest {
    @Test
    public void fib() {
        int n = 45;
        long p = 1, q = 1, r = 1;
        for (int i = 2; i <= n; i++) {
            p = q;
            q = r;
            r = p + q;
            if(r > Integer.MAX_VALUE) {
//                throw new ValueException("Integer.MAX_VALUE");
            }
        }
        System.out.println("n: " + n);
        System.out.println("f(n - 2): " + p);
        System.out.println("f(n - 1): " + q);
        System.out.println("f(n): " + r);
    }
}
