#!/usr/bin/sh
case $1 in
  hs) cd /c/Work/git_repo/a || cd .
  ;;
  hdev) cd /c/Work/git_repo/b || cd .
  ;;
  hdoc) cd /c/Work/git_repo/c || cd .
  ;;
  *) echo "Usage: sd hs|hdev|az|hdoc|hprod|r3|soa"
  ;;
esac