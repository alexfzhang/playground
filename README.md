# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

select *
from sys.user_procedures
;

select *
from sys.user_source
where text like '%alexfzhang%'
;

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

正則表達式搜索中午字符：`[\u2E80-\uFE4F]+`

## Tempaltes
Linked List Setup
```java
private static class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    static ListNode create(int... numbers) {
        ListNode h;
        if (numbers.length > 0) {
            h = new ListNode(numbers[0]);
            ListNode p = h;
            for (int i = 1; i < numbers.length; i++) {
                p.next = new ListNode(numbers[i]);
                p = p.next;
            }
        } else {
            h = null;
        }
        return h;
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListNode listNode = (ListNode) o;
        ListNode b = this;
        while (null != b && null != listNode) {
            if (b.val != listNode.val) {
                return false;
            }
            b = b.next;
            listNode = listNode.next;
        }
        return null == b && null == listNode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(val, next);
    }

    @Override
    public String toString() {
        ListNode a = this;
        StringBuilder s = new StringBuilder();
        while (null != a) {
            s.append(a.val).append(",");
            a = a.next;
        }
        s.setLength(s.length() - 1);
        return s.toString();
    }
}
private static class Input {
    private final ListNode l;
    private final ListNode expected;

    public Input(ListNode l, ListNode expected) {
        this.l = l;
        this.expected = expected;
    }

    public ListNode getL() {
        return l;
    }

    public ListNode getExpected() {
        return expected;
    }
}
    private static Input[] createInput() {
        return new Input[]{};
    }
    @ParameterizedTest
    @MethodSource("createInput")
    public void rotateRight(Input input) {
    }
}
```
# Hive 3.2
```sql
select id, tm, dr, duration, steps from (
  select id, tm, dr, last_value(tm) over (partition by id, gid order by tm) - first_value(tm) over (partition by id, gid order by tm) as duration,
    count(*) over(partition by id, gid order by tm rows between unbounded preceding and unbounded following) as steps,
    row_number() over(partition by id, gid order by tm) as rn,
    dense_rank() over(partition by id order by gid) as session_group_number
  from (
    select id, tm, dr, max(f) over(partition by id order by tm) as gid
    from (
      select id, tm, dr, case when dr > 30 then row_number() over(partition by id order by tm) else 0 end f
      from t
    )
  )
)
where steps = rn
;
```
```
1	a123	1	10
2	a123	2	20
3	a123	3	35
4	a123	4	4
5	a123	5	25
6	a123	6	15
7	a123	7	35
8	a123	8	12
9	a123	9	11
10	a123	10	35
11	a123	11	5
12	a123	12	2
13	a123	13	6
14	a123	14	8
15	a124	15	10
16	a124	16	20
17	a124	17	35
18	a124	18	4
19	a124	19	25
20	a124	20	15
21	a124	21	35
22	a124	22	12
23	a124	23	11
24	a124	24	35
25	a124	25	5
26	a125	26	10
27	a125	27	20
28	a125	28	35
29	a125	29	4
30	a125	30	25
31	a125	31	15
32	a125	32	35
33	a125	33	12
34	a125	34	11
```
